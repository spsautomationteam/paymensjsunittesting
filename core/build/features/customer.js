"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Customer = {
    name: 'customer',
    data: {
        email: void 0,
        telephone: void 0,
        fax: void 0,
        ein: void 0,
        ssn: void 0,
        dateOfBirth: void 0,
        license: void 0
    }
};
