import { Mixin } from "../models/mixin";
export interface licenseData {
    number: string;
    stateCode: string;
}
export interface customerData {
    email?: string;
    telephone?: string;
    fax?: string;
    ein?: string;
    ssn?: string;
    dateOfBirth?: string;
    license?: licenseData;
}
export declare const Customer: Mixin<customerData>;
