import { Mixin } from "../models/mixin";
export interface configData {
    allowPartialAuthorization: boolean;
    cardPresent: boolean;
    authorizationCode: string;
    deviceId: string;
    terminalNumber: string;
    serialNumber: string;
    kernelVersion: string;
}
export declare const Config: Mixin<configData>;
