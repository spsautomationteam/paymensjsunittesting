"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Level3 = {
    name: 'level3',
    data: {
        customerNumber: void 0,
        destinationCountryCode: void 0,
        amounts: {
            discount: void 0,
            duty: void 0,
            nationalTax: void 0,
        },
        vat: {
            idNumber: void 0,
            invoiceNumber: void 0,
            amount: void 0,
            rate: void 0
        }
    }
};
