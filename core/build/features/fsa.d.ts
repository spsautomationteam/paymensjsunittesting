import { Mixin } from "../models/mixin";
export declare type iiasEnum = 'Verified' | 'NotVerified' | 'Exempt';
export interface fsaAmountsData {
    healthCare: string;
    prescription: string;
    clinic: string;
    dental: string;
    vision: string;
}
export interface fsaData {
    iiasVerification: iiasEnum;
    amounts: fsaAmountsData;
}
export declare const FSA: Mixin<fsaData>;
