import { Mixin } from "../models/mixin";
export interface amountsData {
    discount: string;
    duty: string;
    nationalTax: string;
}
export interface vatData {
    idNumber: string;
    invoiceNumber: string;
    amount: string;
    rate: string;
}
export interface level3Data {
    customerNumber: string;
    destinationCountryCode: string;
    amounts: amountsData;
    vat: vatData;
}
export declare const Level3: Mixin<level3Data>;
