"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Config = {
    name: 'config',
    data: {
        allowPartialAuthorization: void 0,
        cardPresent: void 0,
        authorizationCode: void 0,
        deviceId: void 0,
        terminalNumber: void 0,
        serialNumber: void 0,
        kernelVersion: void 0
    }
};
