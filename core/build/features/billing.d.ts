import { Mixin } from "../models/mixin";
export interface nameData {
    first: string;
    middle: string;
    last: string;
    suffix: string;
}
export interface billingData {
    name: string | nameData;
    address: string;
    addressLine1?: string;
    addressLine2?: string;
    city: string;
    state: string;
    postalCode: string;
    country?: string;
}
export declare const Billing: Mixin<billingData>;
