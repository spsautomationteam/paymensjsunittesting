import { Mixin } from "../models/mixin";
export interface nameData {
    first: string;
    middle: string;
    last: string;
    suffix: string;
}
export interface shippingData {
    name: string | nameData;
    address: string;
    city: string;
    state: string;
    postalCode: string;
    country?: string;
}
export declare const Shipping: Mixin<shippingData>;
