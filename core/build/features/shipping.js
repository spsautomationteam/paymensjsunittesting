"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shipping = {
    name: 'shipping',
    data: {
        name: void 0,
        address: void 0,
        city: void 0,
        state: void 0,
        postalCode: void 0,
        country: void 0,
    }
};
