import { Mixin } from "../models/mixin";
export interface postbackData {
    url: string;
    callback: (xhr: XMLHttpRequest) => any;
}
export declare const Postback: Mixin<postbackData>;
