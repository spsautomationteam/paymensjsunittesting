import { Mixin } from "../models/mixin";
export interface recurringScheduleData {
    amount: string;
    frequency: string;
    interval: string;
    nonBusinessDaysHandling: string;
    startDate: string;
    totalCount: string;
    groupId: string;
}
export interface recurringData {
    isRecurring: boolean;
    recurringSchedule: recurringScheduleData;
}
export declare const Recurring: Mixin<recurringData>;
