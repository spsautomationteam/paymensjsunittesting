"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FSA = {
    name: 'fsa',
    data: {
        iiasVerification: void 0,
        amounts: {
            healthCare: void 0,
            prescription: void 0,
            clinic: void 0,
            dental: void 0,
            vision: void 0,
        }
    }
};
