"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Recurring = {
    name: 'recurring',
    data: {
        isRecurring: void 0,
        recurringSchedule: {
            amount: void 0,
            frequency: void 0,
            interval: void 0,
            nonBusinessDaysHandling: void 0,
            startDate: void 0,
            totalCount: void 0,
            groupId: void 0,
        }
    }
};
