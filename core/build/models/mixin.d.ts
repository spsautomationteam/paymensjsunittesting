export interface Mixin<T> {
    [key: string]: any;
    name: string;
    data: T;
}
