import { Mixin } from "./mixin";
export interface Recipe {
    methods: Mixin<any>[];
    operations: Mixin<any>[];
    features: Mixin<any>[];
    options?: any;
    uiOptions?: any;
    callback?: any;
}
