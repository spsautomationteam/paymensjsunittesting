export declare type AvsResult = 'EXACT' | 'PARTIAL' | 'NONE';
export declare type CvvResult = 'EXACT' | 'NONE';
export declare type ProcessingResult = 'APPROVED' | 'ERROR' | 'DECLINED' | 'UNKNOWN';
export interface parsedResponse {
    err: any;
    xhr: XMLHttpRequest;
    data: string;
    hash: string;
    parsed: object;
    processingResult: ProcessingResult;
    avsResult: AvsResult;
    cvvResult: CvvResult;
}
export declare const parseCallback: (err: any, xhr: XMLHttpRequest, data: string, hash: string) => parsedResponse;
