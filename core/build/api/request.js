"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("../utility/http");
const arrays_1 = require("../utility/arrays");
const logging_1 = require("../utility/logging");
let log;
const addHeaders = (xhr, config) => {
    log(`Adding headers`);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("clientVersion", "0.3.13".replace(/\./g, ''));
    !!config.options.auth['clientId'] && xhr.setRequestHeader("clientId", config.options.auth['clientId']);
    !!config.options.auth['authKey'] && xhr.setRequestHeader("authKey", config.options.auth['authKey']);
};
const getUrl = (config) => {
    let BASE_URL = 'https://api-qa.sagepayments.com/paymentsjs/v2';
    if (!config.options || !config.options.auth || !config.options.auth.environment || config.options.auth.environment.toLowerCase() === 'cert') {
        BASE_URL = BASE_URL.replace("api.sage", "api-cert.sage");
    }
    return `${BASE_URL}/api/${arrays_1.asArray(config.operations)[0].name}/${arrays_1.asArray(config.methods)[0].name}`;
};
const getPayload = (mixins, config) => {
    log(`Creating payload from mixins: ${arrays_1.getMixinNamesOnly(mixins).join(', ')}`);
    const ret = {};
    mixins.forEach(m => ret[m.name.toLowerCase()] = m.data);
    ret["auth"] = (!!config.options && config.options.auth) || null;
    return JSON.stringify(ret);
};
const sendRequest = (url) => (config) => (payload) => (callback) => {
    const xhr = http_1.getPostRequest(url);
    let hash;
    let result;
    addHeaders(xhr, config);
    xhr.onreadystatechange = () => xhr.readyState === xhr.DONE && callback(xhr);
    try {
        log(`Sending!`);
        xhr.send(payload);
    }
    catch (ex) {
        log(`Exception: ${ex}`);
        hash = xhr.getResponseHeader('responseHash') || '';
        result = xhr.responseText || ex.toString();
        config.callback && typeof config.callback === "function" && config.callback(true, xhr, result, hash);
    }
};
const clientFirstConfigOverride = (config, serverResp) => {
    const _config = config;
    Object.keys(serverResp).forEach(name => {
        if (name === 'auth') {
            return;
        }
        const opIndex = arrays_1.asArray(_config.operations).findIndex(m => m.name === name);
        const ftIndex = arrays_1.asArray(_config.features).findIndex(m => m.name === name);
        if (opIndex > -1) {
            _config.operations = arrays_1.asArray(_config.operations);
            _config.operations[opIndex].data = serverResp[name];
        }
        if (ftIndex > -1) {
            _config.features = arrays_1.asArray(_config.features);
            _config.features[ftIndex].data = serverResp[name];
        }
        if (opIndex === -1 && ftIndex === -1) {
            _config.features = arrays_1.asArray(_config.features);
            _config.features.push({ name, data: serverResp[name] });
        }
    });
    return _config;
};
const doClientFirstAuth = (config) => {
    log(`Sending auth request to ${config.options.auth.url}`);
    const payload = getPayload([...arrays_1.asArray(config.operations), ...arrays_1.asArray(config.features)], config);
    const cb = (xhr) => {
        log(`Received auth response: ${xhr.responseText}`);
        let _config = Object.assign({}, config);
        try {
            const resp = JSON.parse(xhr.responseText);
            _config.options.auth = Object.assign({}, _config.options.auth, resp.auth);
            _config = clientFirstConfigOverride(_config, resp);
            doApiRequest(_config);
        }
        catch (ex) {
            log(`Exception: ${ex}`);
            _config.callback(true, xhr, ex, '');
        }
    };
    sendRequest(config.options.auth.url)(config)(payload)(cb);
};
const doApiRequest = (config) => {
    log(`Sending request to API`);
    const payload = getPayload([...arrays_1.asArray(config.operations), ...arrays_1.asArray(config.methods), ...arrays_1.asArray(config.features)], config);
    const cb = (xhr) => {
        log(`Received API response: ${xhr.responseText}`);
        const hash = xhr.getResponseHeader('responseHash');
        const pb = arrays_1.findMixinByName(config.features)('postback');
        pb && doPostback(pb)(xhr);
        config.callback && typeof config.callback === "function" && config.callback((xhr.status >= 400), xhr, xhr.responseText, hash);
    };
    sendRequest(getUrl(config))(config)(payload)(cb);
};
const doPostback = (postback) => (xhr) => {
    const { url, callback } = postback.data;
    log(`Sending postback to ${url}`);
    if (url) {
        const hash = xhr.getResponseHeader('responseHash');
        const pbXhr = http_1.getPostRequest(url);
        pbXhr.onreadystatechange = () => { (pbXhr.readyState === pbXhr.DONE) && callback(pbXhr); };
        try {
            pbXhr.setRequestHeader('responseHash', hash);
            pbXhr.send(xhr.responseText);
        }
        catch (ex) {
            log(`Exception: ${ex}`);
            callback && typeof callback === 'function' && callback(pbXhr);
        }
    }
};
exports.Send = (_config) => {
    log = logging_1.getLogger('core/api/request')(_config);
    log('Initializing request module');
    // deep clone to make sure we dont mutate _config.options.auth during client-first, when deleting the url and adding the authkey
    // see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Deep_Clone
    const config = Object.assign({ options: { auth: {} } }, _config, { options: { auth: JSON.parse(JSON.stringify(_config.options.auth)) } });
    config.options.auth.url ? doClientFirstAuth(config) : doApiRequest(config);
};
var response_1 = require("./response");
exports.parseCallback = response_1.parseCallback;
