"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const avsExacts = ["X", "Y", "D", "J", "M", "Q", "V"];
const avsPartials = ["W", "Z", "A", "B", "F", "H", "K", "L", "O", "P", "T"];
exports.parseCallback = (err, xhr, data, hash) => {
    const parsed = (() => { try {
        return JSON.parse(data);
    }
    catch (ex) {
        return void 0;
    } })();
    const exists = (s, root = 'gatewayResponse', type = 'string') => !!parsed && !!parsed[root] && !!parsed[root][s] && typeof parsed[root][s] === type;
    return {
        err,
        xhr,
        data,
        hash,
        parsed,
        processingResult: ((err || (!exists('status') && !exists('vaultResponse', 'gatewayResponse', 'object') && !exists('status', 'vaultResponse', 'number'))) && 'ERROR')
            || (exists('status') && parsed.gatewayResponse.status.toLowerCase() === 'approved' && 'APPROVED')
            || (exists('status') && parsed.gatewayResponse.status.toLowerCase() === 'declined' && 'DECLINED')
            || (exists('vaultResponse', 'gatewayResponse', 'object') && parsed.gatewayResponse.vaultResponse.status === 1 && 'APPROVED')
            || (exists('vaultResponse', 'gatewayResponse', 'object') && parsed.gatewayResponse.vaultResponse.status !== 1 && 'ERROR')
            || (exists('status', 'vaultResponse', 'number') && parsed.vaultResponse.status === 1 && 'APPROVED')
            || (exists('status', 'vaultResponse', 'number') && parsed.vaultResponse.status !== 1 && 'ERROR')
            || 'UNKNOWN',
        avsResult: ((err || !exists('avsResult')) && 'NONE')
            || (avsExacts.indexOf(parsed.gatewayResponse.avsResult.toUpperCase()) > -1 && 'EXACT')
            || (avsPartials.indexOf(parsed.gatewayResponse.avsResult.toUpperCase()) > -1 && 'PARTIAL')
            || 'NONE',
        cvvResult: ((err || !exists('cvvResult')) && 'NONE')
            || ((parsed.gatewayResponse.cvvResult.toUpperCase() === 'M') && 'EXACT')
            || 'NONE'
    };
};
