import { Mixin } from '../models/mixin';
export interface AuthConfig {
    [key: string]: string;
    url?: string;
    clientId?: string;
    requestId?: string;
    authKey?: string;
    salt?: string;
    merchantId?: string;
    environment?: string;
}
export interface DebugConfig {
    [key: string]: any;
    verbose?: boolean;
}
export interface RequestOptions {
    [key: string]: any;
    auth: AuthConfig;
    debug: DebugConfig;
}
export interface RequestConfig {
    [key: string]: any;
    operations: Mixin<any> | Mixin<any>[];
    methods: Mixin<any> | Mixin<any>[];
    features?: Mixin<any> | Mixin<any>[];
    options: RequestOptions;
    callback: (err: any, xhr: any, data: string, hash: string) => any;
}
export declare const Send: (_config: RequestConfig) => void;
export { parseCallback } from './response';
