import { Mixin } from "../models/mixin";
export interface vaultData {
    operation: string;
    token?: string;
}
export declare const Vault: Mixin<vaultData>;
