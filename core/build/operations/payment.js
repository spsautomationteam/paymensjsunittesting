"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Payment = {
    name: 'payment',
    data: {
        preAuth: void 0,
        orderNumber: void 0,
        totalAmount: void 0,
        shippingAmount: void 0,
        taxAmount: void 0,
        tipAmount: void 0,
    }
};
