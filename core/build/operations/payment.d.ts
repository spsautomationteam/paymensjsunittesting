import { Mixin } from "../models/mixin";
export interface paymentData {
    totalAmount: string;
    preAuth?: boolean;
    taxAmount?: string;
    shippingAmount?: string;
    orderNumber?: string;
    tipAmount?: string;
}
export declare const Payment: Mixin<paymentData>;
