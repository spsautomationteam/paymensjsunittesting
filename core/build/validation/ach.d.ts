export declare const isValidRouting: (s: string) => boolean;
export declare const isValidAccount: (s: string) => boolean;
export declare const isValidType: (s: string) => boolean;
