"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const date_1 = require("../utility/date");
const predicates_1 = require("../utility/predicates");
const regex_1 = require("../utility/regex");
const mod10_1 = require("../utility/mod10");
const arrays_1 = require("../utility/arrays");
const cardTypes = {
    '2': 'MasterCard',
    '3': 'American Express',
    '4': 'Visa',
    '5': 'MasterCard',
    '6': 'Discover',
};
exports.getCardType = (s) => s && cardTypes[s.toString()] || '';
const cardStartsWith = (testFor) => (testThis) => arrays_1.asArray(testFor).some(n => testThis.toString().charAt(0) === n);
exports.isAmex = (s) => cardStartsWith('3')(s);
exports.isVisa = (s) => cardStartsWith('4')(s);
exports.isMasterCard = (s) => cardStartsWith(['2', '5',])(s);
exports.isDiscover = (s) => cardStartsWith('6')(s);
exports.isAllowedCard = (opts = { amex: true, disc: true, visa: true, mc: true }) => (cc) => predicates_1.testConditions([
    [cc, cc => !exports.isAmex(cc) || opts.amex !== false],
    [cc, cc => !exports.isVisa(cc) || opts.visa !== false],
    [cc, cc => !exports.isMasterCard(cc) || opts.mc !== false],
    [cc, cc => !exports.isDiscover(cc) || opts.disc !== false]
]);
exports.isValidCardNumber = (opts = { amex: true, disc: true, visa: true, mc: true }) => (cc) => predicates_1.testConditions([
    [cc, exports.isAllowedCard(opts)],
    [cc, regex_1.testIsNumeric],
    [cc, cc => cc.length === 16 || exports.isAmex(cc) && cc.length === 15],
    [cc, mod10_1.test]
]);
// expiration:
exports.isValidMonthString = (m) => predicates_1.testConditions([
    // string is 1 char, or two chars where first is '0' or '1'
    [m, m => m.length === 1 || (m.length === 2 && ['0', '1'].indexOf(m[0]) > -1)],
    // month is between 1 and 12: 
    [m, m => parseInt(m) >= 1 && parseInt(m) <= 12],
]);
exports.isValidYearString = (y) => predicates_1.testConditions([
    // string is 2 chars, or four chars beginning wtih '20'.
    // ... not y3k-compliant. (╯°□°）╯︵ ┻━┻
    [y, y => y.length === 2 || y.length === 4 && y.startsWith('20')],
]);
exports.isValidExpirationDate = (exp) => {
    const m = exp[0].toString();
    const y = exp[1].toString();
    return predicates_1.testConditions([
        [m, regex_1.testIsNumeric],
        [y, regex_1.testIsNumeric],
        [m, exports.isValidMonthString],
        [y, exports.isValidYearString],
        [y, (y) => !date_1.yearIsInPast(y)],
    ]) && !(date_1.yearIsCurrentYear(y) && date_1.monthIsInPast(m)); // <-- effectively two inputs
};
// cvv
exports.isValidCvv = (cvv) => predicates_1.testConditions([
    [cvv, (cvv) => cvv.length === 3 || cvv.length === 4],
    [cvv, regex_1.testIsNumeric]
]);
