export declare const getCardType: (s: string | number) => "" | "Visa" | "MasterCard" | "American Express" | "Discover";
export declare const isAmex: (s: string | number) => boolean;
export declare const isVisa: (s: string | number) => boolean;
export declare const isMasterCard: (s: string | number) => boolean;
export declare const isDiscover: (s: string | number) => boolean;
export declare const isAllowedCard: (opts?: {
    amex: boolean;
    disc: boolean;
    visa: boolean;
    mc: boolean;
}) => (cc: string) => boolean;
export declare const isValidCardNumber: (opts?: {
    amex: boolean;
    disc: boolean;
    visa: boolean;
    mc: boolean;
}) => (cc: string) => boolean;
export declare const isValidMonthString: (m: string) => boolean;
export declare const isValidYearString: (y: string) => boolean;
export declare const isValidExpirationDate: (exp: [string | number, string | number]) => boolean;
export declare const isValidCvv: (cvv: string) => boolean;
