"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const regex_1 = require("../utility/regex");
exports.isValidRouting = (s) => regex_1.testIsNumeric(s) && s.length === 9;
exports.isValidAccount = (s) => regex_1.testIsNumeric(s);
exports.isValidType = (s) => ['checking', 'savings'].indexOf(s.toLowerCase()) > -1;
//export const isValidSecCodeString = (s : string) => ['ppd', 'ccd', 'web'].indexOf(s.toLowerCase()) > -1; 
