export declare const monthIsInPast: (monthToTest: string | number) => boolean;
export declare const getCurrentYear: (testYear?: string | number) => string;
export declare const yearIsCurrentYear: (yearToTest: string | number) => boolean;
export declare const yearIsInPast: (yearToTest: string | number) => boolean;
