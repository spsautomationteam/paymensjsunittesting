export declare const getDelimitingFunction: (newChar: string) => (positions: number[]) => (stringToFormat: string | number) => string;
export declare const getMaskingFunction: (newChar: string) => (positions: number[]) => (stringToFormat: string | number) => string;
export declare const camelCaseToTitle: (s: string) => string;
