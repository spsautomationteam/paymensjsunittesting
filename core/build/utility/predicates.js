"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*

this method takes an array of arrays --

    [ [valueToTest, conditionToTest], [valueToTest, conditionToTest], ... ]

-- and then iterates through calling:

    conditionToTest(valueToTest);

this has at least two benefits:

    1. using "every" forces shortcircuiting
    2. it forces conditions to be concise

*/
exports.testConditions = (c) => c.every(c => c[1](c[0]));
