"use strict";
// TODO: this should be in ui app
Object.defineProperty(exports, "__esModule", { value: true });
const isCtrlOrCmdKey = (event) => event.ctrlKey || event.metaKey; // metaKey is cmd on mac, windows key on windows.
const isNumberKey = (code) => (48 <= code) && (code <= 57);
// Firefox registers an '8' for backspace, and a '0' for other non-printing keys (eg home/end).
// Other browsers are silent in both cases, but don't seem to use 0/8 anywhere else, so this should be okay.
const isFirefoxAndThereforeApparentlyRequiresSpecialTreatment = (code) => (code == 8) || (code == 0);
const adHoc = (code) => isFirefoxAndThereforeApparentlyRequiresSpecialTreatment(code); // && ... any other adhoc tests here
exports.isAllowedKeystroke = (event) => isNumberKey(event.which) || isCtrlOrCmdKey(event) || adHoc(event.which);
