export declare const stripNonNumeric: (s: string) => string;
export declare const stripNonAlphaNumeric: (s: string) => string;
export declare const testIsNumeric: (s: string) => boolean;
export declare const testIsAlphaNumeric: (s: string) => boolean;
