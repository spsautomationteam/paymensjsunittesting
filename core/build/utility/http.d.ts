export declare const getPostRequest: (url: string) => XMLHttpRequest;
export declare const getGetRequest: (url: string) => XMLHttpRequest;
