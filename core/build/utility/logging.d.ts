import { RequestConfig } from "../api/request";
export declare const getClientSession: () => string;
export declare type LogFn = (message: string, level?: string) => boolean;
export declare const getLogger: (name: string) => (config: RequestConfig) => LogFn;
