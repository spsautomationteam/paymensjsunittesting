"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getFormattingFunction = (delimit) => (newChar) => (positions) => (stringToFormat) => stringToFormat.toString()
    .split('')
    .map((oldChar, i) => positions.indexOf(i) > -1 ? `${newChar}${delimit ? oldChar : ''}` : oldChar)
    .join('');
exports.getDelimitingFunction = getFormattingFunction(true);
exports.getMaskingFunction = getFormattingFunction(false);
exports.camelCaseToTitle = (s) => s.toLowerCase()
    .split('')
    .reduce((acc, val, i) => `${acc}${val === s[i] && (i ? val : val.toUpperCase()) || ' ' + s[i]}`, '');
