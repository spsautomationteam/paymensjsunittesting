"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.asArray = (x) => (Array.isArray(x) && x) // if it's an array of Ts, return the array
    || [x]; // otherwise, if it's a single T, return an array that contains only the single T
exports.findMixinByName = (haystack) => (needle) => exports.asArray(haystack).find(m => m.name === needle);
exports.getMixinNamesOnly = (haystack) => exports.asArray(haystack).map(m => m.name);
