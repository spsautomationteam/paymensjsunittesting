"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = require("./http");
// we need to make up some arbitrary identifier for client sessions.
// timestamp + pseudorandom string has a sufficiently-small collision space:
const sessionId = +new Date + (Math.random() + 1).toString(36).substring(7);
exports.getClientSession = () => sessionId;
// maintain a SINGLE repository of log messages from ALL logging modules:
const logRepo = [];
// the first module to call getLogger() gets this going on an interval:
let logging = false;
const sendLogs = (clientId) => {
    const logs = (() => {
        const ret = [];
        logRepo.forEach(() => ret.push(logRepo.shift()));
        return ret.join(' | ');
    })();
    const xhr = http_1.getGetRequest('https://api-qa.sagepayments.com/paymentsjs/v2/api/logs');
    xhr.setRequestHeader("clientVersion", "0.3.13".replace(/\./g, ''));
    xhr.setRequestHeader("clientId", clientId);
    xhr.setRequestHeader("clientState", logs.replace(/\r?\n|\r/g, ''));
    xhr.setRequestHeader("clientSession", sessionId);
    xhr.send();
};
exports.getLogger = (name) => (config) => {
    /*
        -> const o = { p: 'foo', q: 'bar' }
        -> const { p, q } = o;
        -> console.log(p);
        <- 'foo'

        if this syntax is unfamiliar or unintuitive, please google "es6 destructuring".
        if it's 2020 and you're on like es10 then APPRECIATE YOUR FUTURISTIC LUXURIES.

        love,
        2017
    */
    const defaultAuthOptions = { clientId: '' };
    const { clientId } = (!!config && !!config.options && !!config.options.auth && Object.assign(defaultAuthOptions, config.options.auth))
        || defaultAuthOptions;
    const defaultDebugOptions = { verbose: false, logInterval: 10000 };
    const { verbose, logInterval } = (!!config && !!config.options && !!config.options.debug && Object.assign(defaultDebugOptions, config.options.debug))
        || defaultDebugOptions;
    const log = (s, level = 'log') => {
        const msg = `${name}: ${s}`;
        if (logRepo[logRepo.length - 1] !== msg) {
            logRepo.push(msg);
            verbose && console[level](msg);
        }
        return true; // for &&-chaining
    };
    logging = logging
        || (!!clientId && logInterval > 0 && log('Initializing logger.') && setInterval(() => { logRepo.length > 0 && sendLogs(clientId); }, logInterval))
        || (!clientId && log('Couldn\'t find options.auth.clientId -- logs will only be available in the browser console.', 'warn'))
        || (logInterval <= 0 && log('options.auth.logInterval set to zero -- no further logging.', 'warn') && false);
    return logging ? log : () => false;
};
