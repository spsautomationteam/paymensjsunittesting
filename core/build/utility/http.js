"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getRequest = (method) => (url) => {
    const req = new XMLHttpRequest();
    req.timeout = 60000;
    req.open(method, url, true);
    return req;
};
exports.getPostRequest = (url) => getRequest('POST')(url);
exports.getGetRequest = (url) => getRequest('GET')(url);
