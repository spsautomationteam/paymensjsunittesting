import { Mixin } from "../models/mixin";
export declare const asArray: <T>(x: T | T[]) => T[];
export declare const findMixinByName: (haystack: Mixin<any> | Mixin<any>[]) => (needle: string) => Mixin<any>;
export declare const getMixinNamesOnly: (haystack: Mixin<any> | Mixin<any>[]) => any[];
