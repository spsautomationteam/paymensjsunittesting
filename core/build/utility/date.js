"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const now = new Date();
const currentMonth = (now.getMonth() + 1).toString();
const currentYearFourChars = now.getFullYear().toString();
const currentYearTwoChars = currentYearFourChars.slice(2, 4);
exports.monthIsInPast = (monthToTest) => parseInt(monthToTest) < parseInt(currentMonth);
exports.getCurrentYear = (testYear = 'XX') => (testYear.toString()).length === 4 ? currentYearFourChars : currentYearTwoChars;
exports.yearIsCurrentYear = (yearToTest) => parseInt(yearToTest) === parseInt(exports.getCurrentYear(yearToTest.toString())); // <-- passing yearToTest thru getCurrentYear standardizes the length for comparison
exports.yearIsInPast = (yearToTest) => parseInt(yearToTest) < parseInt(exports.getCurrentYear(yearToTest.toString()));
