"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.test = (s) => {
    // http://www.the-art-of-web.com/javascript/validate/3/
    let sum = 0;
    const numdigits = s.length;
    const parity = numdigits % 2;
    for (let i = 0; i < numdigits; i++) {
        let digit = parseInt(s.charAt(i));
        if (i % 2 == parity)
            digit *= 2;
        if (digit > 9)
            digit -= 9;
        sum += digit;
    }
    return ((sum % 10) == 0);
};
