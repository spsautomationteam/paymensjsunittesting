"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// basal function:
const getStripper = (pattern) => (s) => s.replace(pattern, '');
// 1st-order functions:
exports.stripNonNumeric = (s) => getStripper(/\D/g)(s);
exports.stripNonAlphaNumeric = (s) => getStripper(/\W/g)(s);
// 2nd-order functions:
exports.testIsNumeric = (s) => s.length > 0 && exports.stripNonNumeric(s) === s;
exports.testIsAlphaNumeric = (s) => s.length > 0 && exports.stripNonAlphaNumeric(s) === s;
