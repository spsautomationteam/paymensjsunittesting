import { Mixin } from '../models/mixin';
export interface achData {
    routingNumber: string;
    accountNumber: string;
    type: string;
    secCode?: string;
}
export declare const ACH: Mixin<achData>;
