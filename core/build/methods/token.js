"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VaultToken = {
    name: 'token',
    data: {
        token: void 0,
        cvv: void 0,
        secCode: void 0,
    }
};
