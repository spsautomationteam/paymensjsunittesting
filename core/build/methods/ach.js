"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ACH = {
    name: 'ach',
    data: {
        routingNumber: void 0,
        accountNumber: void 0,
        type: void 0,
        secCode: void 0
    }
};
