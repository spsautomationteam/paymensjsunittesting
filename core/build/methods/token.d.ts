import { Mixin } from '../models/mixin';
export interface tokenData {
    token: string;
    cvv?: string;
    secCode?: string;
}
export declare const VaultToken: Mixin<tokenData>;
