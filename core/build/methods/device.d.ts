import { Mixin } from '../models/mixin';
export interface deviceData {
    type: string;
    data: string;
}
export declare const Device: Mixin<deviceData>;
