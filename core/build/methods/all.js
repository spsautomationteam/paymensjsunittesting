"use strict";
// this file and ui/components/common/paymentMethod(+Select) are oddly-coupled.
// order matters!
Object.defineProperty(exports, "__esModule", { value: true });
// the first export is used as the default payment type:
var card_1 = require("./card");
exports.CreditCard = card_1.CreditCard;
// then put any methods that don't have a correponding ui form:
var token_1 = require("./token");
exports.VaultToken = token_1.VaultToken;
// then the rest:
var ach_1 = require("./ach");
exports.ACH = ach_1.ACH;
var device_1 = require("./device");
exports.Device = device_1.Device;
var masterpass_1 = require("./masterpass");
exports.Masterpass = masterpass_1.Masterpass;
