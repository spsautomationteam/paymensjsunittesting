import { Mixin } from '../models/mixin';
export interface cardData {
    number: string;
    expiration: string;
    cvv?: string;
}
export declare const CreditCard: Mixin<cardData>;
