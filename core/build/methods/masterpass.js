"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Masterpass = {
    name: 'masterpass',
    data: {
        requestToken: void 0,
        callbackUrl: void 0,
        failureCallback: void 0,
        cancelCallback: void 0,
        successCallback: void 0,
        merchantCheckoutId: void 0,
        verifierToken: void 0,
        checkoutUrl: void 0,
        allowedCardTypes: void 0,
        version: void 0
    }
};
