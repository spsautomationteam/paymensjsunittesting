import { Mixin } from '../models/mixin';
export declare type masterpassCardTypes = 'master' | 'amex' | 'diners' | 'discover' | 'maestro' | 'visa';
export interface masterpassData {
    callbackUrl: string;
    requestToken: string;
    merchantCheckoutId: string;
    allowedCardTypes: string[] | masterpassCardTypes[];
    version: string;
    verifierToken: string;
    checkoutUrl: string;
    successCallback: (any: any) => any;
    failureCallback: (any: any) => any;
    cancelCallback: (any: any) => any;
}
export declare const Masterpass: Mixin<masterpassData>;
