export const testMixinProps = (obj = {}, shallowFields = [], deepFields = {}) => {
    shallowFields.forEach(f => {
        expect(obj.hasOwnProperty(f)).toBe(true);
        if (Object.keys(deepFields).indexOf(f) > -1) {
            testMixinProps(obj[f], deepFields[f], []) //<-- note that this can only test one-deep. this is intentional; complexity in module.data should be considered a code smell.
        } else {
            expect(obj[f]).toBeUndefined();
        }
    });
    expect(Object.getOwnPropertyNames(obj).length).toBe(shallowFields.length + Object.keys(deepFields).length);
}