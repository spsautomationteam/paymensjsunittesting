import { CreditCard as mixinTest }  from '../../methods/card'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'card';
const fields = [ 'number', 'expiration', 'cvv'];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

test('mixin has the typeof name', () => {
    expect(typeof mixinTest).toBe('object');
})

test('mixin has the data and it has number declared as', () => {
    expect(typeof mixinTest.data.number).toBe('string');
})

test('mixin has the data and it has number declared as undefined', () => {
    expect(mixinTest.data.number).toBe(undefined);
})

test('mixin has the data and it has expiration declared as undefined', () => {
    expect(mixinTest.data.expiration).toBe(undefined);
})

test('mixin has the data and it has cvv declared as undefined', () => {
    expect(mixinTest.data.cvv).toBe(undefined);
})




