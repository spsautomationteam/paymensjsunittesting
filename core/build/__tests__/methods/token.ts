import { VaultToken as mixinTest }  from '../../methods/token'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'token';
const fields = [ 'token', 'cvv', 'secCode' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

