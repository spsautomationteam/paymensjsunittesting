import { ACH as mixinTest }  from '../../methods/ach'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'ach';
const fields = [ 'routingNumber', 'accountNumber', 'type', 'secCode' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

