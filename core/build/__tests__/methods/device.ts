import { Device as mixinTest }  from '../../methods/device'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'device';
const fields = [ 'type', 'data' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

