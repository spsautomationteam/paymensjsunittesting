import { Masterpass as mixinTest }  from '../../methods/masterpass'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'masterpass';
const fields = [ 'callbackUrl', 'requestToken', 'merchantCheckoutId', 'allowedCardTypes', 'version', 'verifierToken', 'checkoutUrl', 'successCallback', 'failureCallback', 'cancelCallback' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

