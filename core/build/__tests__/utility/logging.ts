import { getLogger } from '../../utility/logging'

test('getLogger is a curried function that eventually returns true', () => { 
    let l = getLogger;
    expect(typeof l).toBe('function');
    l = l('some/module/name');
    expect(typeof l).toBe('function');
    l = l({});
    expect(typeof l).toBe('function');
    l = l('some message');
    expect(l).toBe(true);
});