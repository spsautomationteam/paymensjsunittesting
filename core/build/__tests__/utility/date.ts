import { monthIsInPast, getCurrentYear, yearIsCurrentYear, yearIsInPast } from '../../utility/date'

const now = new Date();
const thisMonth =  (now.getMonth() + 1);
const thisYear = now.getFullYear();

test('monthIsInPast tests if a given month is in the past', () => {
    expect(monthIsInPast(thisMonth - 1)).toBe(true);
    expect(monthIsInPast(thisMonth)).toBe(false);
    expect(monthIsInPast(thisMonth + 1)).toBe(false);
})

test('monthIsInPast can operate on a string', () => {
    expect(monthIsInPast((thisMonth - 1).toString())).toBe(true);
    expect(monthIsInPast(thisMonth.toString())).toBe(false);
    expect(monthIsInPast((thisMonth + 1).toString())).toBe(false);
})

test('getCurrentYear returns a string', () => {
    expect(typeof getCurrentYear()).toBe('string');
})

test('getCurrentYear returns a string', () => {
    expect(typeof getCurrentYear()).toBe('string');
})

test('getCurrentYear defaults to returning the last two current year', () => {
    expect(getCurrentYear()).toBe(thisYear.toString().slice(2, 4));
})

test('getCurrentYear returns the full year when prompted by a formatter argument', () => {
    expect(getCurrentYear()).toBe(thisYear.toString().slice(2, 4));
    expect(getCurrentYear('XX')).toBe(thisYear.toString().slice(2, 4));
    expect(getCurrentYear('XXXX')).toBe(thisYear.toString());
})

test('yearIsCurrentYear returns a boolean', () => {
    expect(typeof yearIsCurrentYear(thisYear)).toBe('boolean');
})

test('yearIsCurrentYear compares the argument to the current year', () => {
    expect(yearIsCurrentYear(thisYear - 1)).toBe(false);
    expect(yearIsCurrentYear(thisYear    )).toBe(true);
    expect(yearIsCurrentYear(thisYear + 1)).toBe(false);
    // these ensure the first two numbers are relevant (eg, 2017 != 2117):
    expect(yearIsCurrentYear(thisYear + 100)).toBe(false);
    expect(yearIsCurrentYear(thisYear - 100)).toBe(false);
})

test('yearIsCurrentYear also accepts strings', () => {
    // prev test, .toString()
    expect(yearIsCurrentYear((thisYear - 1)  .toString())).toBe(false);
    expect(yearIsCurrentYear((thisYear    )  .toString())).toBe(true);
    expect(yearIsCurrentYear((thisYear + 1)  .toString())).toBe(false);
    expect(yearIsCurrentYear((thisYear + 100).toString())).toBe(false);
    expect(yearIsCurrentYear((thisYear - 100).toString())).toBe(false);

})

test('yearIsCurrentYear accepts the short or full year', () => {
    // prev *two* tests, adjusted to short year

    // numbers:                             v vvvv
    expect(yearIsCurrentYear(thisYear - 1   - 2000)).toBe(false);
    expect(yearIsCurrentYear(thisYear       - 2000)).toBe(true);
    expect(yearIsCurrentYear(thisYear + 1   - 2000)).toBe(false);

    // strings:
    expect(yearIsCurrentYear((thisYear - 1)  .toString().slice(2,4))).toBe(false);
    expect(yearIsCurrentYear((thisYear    )  .toString().slice(2,4))).toBe(true);
    expect(yearIsCurrentYear((thisYear + 1)  .toString().slice(2,4))).toBe(false);
    expect(yearIsCurrentYear((thisYear + 100).toString().slice(2,4))).toBe(true);
    expect(yearIsCurrentYear((thisYear - 100).toString().slice(2,4))).toBe(true);
})

test('yearIsInPast returns a boolean', () => {
    expect(typeof yearIsInPast(thisYear)).toBe('boolean');
})

test('yearIsInPast compares the argument to the current year', () => {
    expect(yearIsInPast(thisYear - 1)).toBe(true);
    expect(yearIsInPast(thisYear    )).toBe(false);
    expect(yearIsInPast(thisYear + 1)).toBe(false);
    // these ensure the first two numbers are relevant (eg, 2017 != 2117):
    expect(yearIsInPast(thisYear + 100)).toBe(false);
    expect(yearIsInPast(thisYear - 100)).toBe(true);
})

test('yearIsInPast also accepts strings', () => {
    // prev test, .toString()
    expect(yearIsInPast((thisYear - 1)  .toString())).toBe(true);
    expect(yearIsInPast((thisYear    )  .toString())).toBe(false);
    expect(yearIsInPast((thisYear + 1)  .toString())).toBe(false);
    expect(yearIsInPast((thisYear + 100).toString())).toBe(false);
    expect(yearIsInPast((thisYear - 100).toString())).toBe(true);

})

test('yearIsInPast accepts the short or full year', () => {
    // prev *two* tests, adjusted to short year

    // numbers:
    expect(yearIsInPast(thisYear - 1   - 2000)).toBe(true);
    expect(yearIsInPast(thisYear       - 2000)).toBe(false);
    expect(yearIsInPast(thisYear + 1   - 2000)).toBe(false);

    // strings:
    expect(yearIsInPast((thisYear - 1)  .toString().slice(2,4))).toBe(true);
    expect(yearIsInPast((thisYear    )  .toString().slice(2,4))).toBe(false);
    expect(yearIsInPast((thisYear + 1)  .toString().slice(2,4))).toBe(false);
    expect(yearIsInPast((thisYear + 100).toString().slice(2,4))).toBe(false);
    expect(yearIsInPast((thisYear - 100).toString().slice(2,4))).toBe(false);
})

test('yearIsInFuture is derivable from negated disjunct of yearIsInPast/yearIsCurrentYear (or equiv)', () => {
    const yearIsInFuture = y => !(yearIsInPast(y) || yearIsCurrentYear(y));
    const futureYears = [
        thisYear + 1, thisYear + 100,
        (thisYear + 1).toString(), (thisYear + 100).toString(),
    ];
    futureYears.forEach(y => expect(yearIsInFuture(y)).toBe(true))
})