import { getDelimitingFunction, getMaskingFunction, camelCaseToTitle } from '../../utility/strings'

test('getMaskingFunction replaces the specified character-positions with the specified character', () => {
    const maskChar = 'X';
    const maskFn = getMaskingFunction(maskChar);
    expect(maskFn([0, 2, 4])('012345')).toBe('X1X3X5')
    expect(maskFn([0, 3, 9])('0123456789')).toBe('X12X45678X')
})

test('getMaskingFunction ignores positions outside the string length', () => {
    const maskChar = 'X';
    const maskFn = getMaskingFunction(maskChar);
    expect(maskFn([9])('012345')).toBe('012345');
})

test('getMaskingFunction doesn\'t freak out when oldChar = newChar', () => {
    const maskChar = 'X';
    const maskFn = getMaskingFunction(maskChar);
    expect(maskFn([2])('XXX')).toBe('XXX');
})

test('getDelimitingFunction inserts the specified characters at the specified positions', () => {
    const delimChar = '-';
    const delimFn = getDelimitingFunction(delimChar);
    expect(delimFn([1, 3, 5])('012345')).toBe('0-12-34-5')
    expect(delimFn([0, 3, 9])('0123456789')).toBe('-012-345678-9')
})

test('getDelimitingFunction ignores positions outside the string length', () => {
    const delimChar = '-';
    const delimFn = getDelimitingFunction(delimChar);
    expect(delimFn([9])('012345')).toBe('012345');
})

test('getDelimitingFunction doesn\'t freak out when oldChar = newChar', () => {
    const delimChar = 'X';
    const delimFn = getDelimitingFunction(delimChar);
    expect(delimFn([2])('XXX')).toBe('XXXX');
})

test('camelCaseToTitle converts camel case to title case', () => {
    expect(camelCaseToTitle('foo')).toBe('Foo');
    expect(camelCaseToTitle('fooBar')).toBe('Foo Bar');
    expect(camelCaseToTitle('fooBarBaz')).toBe('Foo Bar Baz');
})

test('camelCaseToTitle handles invalid input', () => {
    // this test is more for change-sensitivity than any particular behavior
    expect(camelCaseToTitle('')).toBe('');
    expect(camelCaseToTitle('foo')).toBe('Foo');
    expect(camelCaseToTitle('foo bar')).toBe('Foo bar');
    expect(camelCaseToTitle('foo bar baz')).toBe('Foo bar baz');
})