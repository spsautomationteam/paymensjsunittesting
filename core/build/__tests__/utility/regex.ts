import * as r from '../../utility/regex';

const numbs : string = '0123456789';
const alpha : string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_';
const other : string = '`~!@#$%^&*()-=+[]\\{}|;:,./<>?\'\"\r\f\t\v    '

test('stripNonNumeric removes all non-numeric characters', () => {
    expect(r.stripNonNumeric(numbs)).toBe(numbs);
    expect(r.stripNonNumeric(numbs + alpha)).toBe(numbs);
    expect(r.stripNonNumeric(numbs + alpha + other)).toBe(numbs);
    expect(r.stripNonNumeric(alpha)).toBe('');
    expect(r.stripNonNumeric(other)).toBe('');
    expect(r.stripNonNumeric('')).toBe('');
})

test('stripNonAlphaNumeric removes all non-alphanumeric characters', () => {
    expect(r.stripNonAlphaNumeric(numbs)).toBe(numbs);
    expect(r.stripNonAlphaNumeric(numbs + other)).toBe(numbs);
    expect(r.stripNonAlphaNumeric(alpha)).toBe(alpha);
    expect(r.stripNonAlphaNumeric(alpha + other)).toBe(alpha);
    expect(r.stripNonAlphaNumeric(numbs + alpha)).toBe(numbs + alpha);
    expect(r.stripNonAlphaNumeric(numbs + alpha + other)).toBe(numbs + alpha);
    expect(r.stripNonAlphaNumeric(other)).toBe('');
    expect(r.stripNonAlphaNumeric('')).toBe('');
})

test('testIsNumeric tests if a string is all numeric characters', () => {
    expect(r.testIsNumeric(numbs)).toBe(true);
    expect(r.testIsNumeric(numbs + alpha + other)).toBe(false);
    expect(r.testIsNumeric(alpha)).toBe(false);
    expect(r.testIsNumeric(other)).toBe(false);
    expect(r.testIsNumeric('')).toBe(false); // ?
})

test('testIsAlphaNumeric tests if a string is all alphanumeric characters', () => {
    expect(r.testIsAlphaNumeric(numbs)).toBe(true);
    expect(r.testIsAlphaNumeric(alpha)).toBe(true);
    expect(r.testIsAlphaNumeric(numbs + alpha)).toBe(true);
    expect(r.testIsAlphaNumeric(numbs + other)).toBe(false);
    expect(r.testIsAlphaNumeric(alpha + other)).toBe(false);
    expect(r.testIsAlphaNumeric(numbs + alpha + other)).toBe(false);
    expect(r.testIsAlphaNumeric(other)).toBe(false);
    expect(r.testIsAlphaNumeric('')).toBe(false); // ?
})