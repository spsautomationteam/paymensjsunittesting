import { getGetRequest, getPostRequest } from '../../utility/http'

// not quite a unit test, not quite an integration test.

const getTestUrl = 'https://httpbin.org/get';
const postTestUrl = 'https://httpbin.org/post';

test('getGetRequest returns an XMLHttpRequest with a timeout of 60s', () => {
    const xhr = getGetRequest(getTestUrl);
    expect(xhr.timeout).toBe(60 * 1000);
});

test('getPostRequest returns an XMLHttpRequest with a timeout of 60s', () => {
    const xhr = getPostRequest(getTestUrl);
    expect(xhr.timeout).toBe(60 * 1000);
});

test('getGetRequest returns an XMLHttpRequest for GET requests to the specified URL', (done) => {
    const xhr = getGetRequest(getTestUrl);
    expect(xhr.readyState).toBe(1); // open
    xhr.onreadystatechange = () => {
        if (xhr.readyState === xhr.DONE) {
            expect(xhr.status).toBe(200);
            expect(JSON.parse(xhr.responseText).url).toBe(getTestUrl)
            done();
        }
    }
    xhr.send();
})

test('getPostRequest returns an XMLHttpRequest for POST requests to the specified URL', (done) => {
    const fakeData = JSON.stringify({ foo: 'bar' });
    const xhr = getPostRequest(postTestUrl);
    expect(xhr.readyState).toBe(1); // open
    xhr.onreadystatechange = () => {
        if (xhr.readyState === xhr.DONE) {
            expect(xhr.status).toBe(200);
            expect(JSON.parse(xhr.responseText).url).toBe(postTestUrl)
            expect(JSON.parse(xhr.responseText).data).toBe(fakeData)
            done();
        }
    }
    xhr.send(fakeData);
})