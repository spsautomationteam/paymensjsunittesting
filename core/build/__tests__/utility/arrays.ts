import { asArray, findMixinByName, getMixinNamesOnly } from '../../utility/arrays';

test('asArray returns an array', () => {
    expect(asArray()).toEqual([void 0]);
})

test('asArray returns an array argument', () => {
    expect(asArray([])).toEqual([]);
    expect(asArray([0])).toEqual([0]);
    expect(asArray(['foo', 0, null])).toEqual(['foo', 0, null]);
})

test('asArray returns a non-array argument as an array', () => {
    expect(asArray(0)).toEqual([0]);
    expect(asArray('foo')).toEqual(['foo']);
    expect(asArray(null)).toEqual([null]);
    expect(asArray({})).toEqual([{}]);
})

test('findMixinByName returns a function', () => {
    expect(typeof findMixinByName()).toEqual('function');
})

test('findMixinByName finds mixins by name', () => {
    const someMixin = { name: 'foo' }
    const anotherMixin = { name: 'bar' }
    const mixinArray = [someMixin, anotherMixin];
    expect(findMixinByName(mixinArray)('foo')).toBe(someMixin);
    expect(findMixinByName(mixinArray)('bar')).toBe(anotherMixin);
})

test('findMixinByName calls asArray on its haystack', () => {
    const someMixin = { name: 'foo' }
    const anotherMixin = { name: 'bar' }
    expect(findMixinByName(someMixin)('foo')).toBe(someMixin);
    expect(findMixinByName(anotherMixin)('bar')).toBe(anotherMixin);
})

test('findMixinByName returns undefined when no matches are found', () => {
    const someMixin = { name: 'foo' }
    const anotherMixin = { name: 'bar' }
    const mixinArray = [someMixin, anotherMixin];
    expect(findMixinByName(someMixin)('baz')).toBe(void 0);
    expect(findMixinByName(anotherMixin)('baz')).toBe(void 0);
    expect(findMixinByName(mixinArray)('baz')).toBe(void 0);
})

test('findMixinByName returns the first result when there are multiple possibilities', () => {
    const someMixin = { name: 'foo' }
    const anotherMixin = Object.assign({}, someMixin);
    const mixinArray = [someMixin, anotherMixin];
    expect(findMixinByName(mixinArray)('foo')).toBe(someMixin);
    expect(findMixinByName(mixinArray)('foo')).not.toBe(anotherMixin);
})

test('getMixinNamesOnly returns an array', () => {
    expect(getMixinNamesOnly([])).toEqual([]);
})

test('getMixinNamesOnly receives mixins and returns an array of their names', () => {
    expect(getMixinNamesOnly([
        { name: 'foo', data: 'x' },
        { name: 'bar', data: 'x' },
    ])).toEqual(['foo', 'bar']);
})

test('getMixinNamesOnly calls asArray on its haystack', () => {
    expect(getMixinNamesOnly({ name: 'foo', data: 'x' })).toEqual(['foo']);
})