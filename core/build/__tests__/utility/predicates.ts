import { testConditions } from '../../utility/predicates';

const isTruthy = x => !!x;
const isFalsy = x => !x;
const isItself = x => x === x;
const isFoo = x => x === 'foo';
const isHalfOfFour = x => x === 4 / 2;
const addToTen = x => (x[0] + x[1]) === 10

test('testConditions evaluates a sequence of subject/predicate pairs and returns a boolean', () => {
    expect(testConditions([[null, () => true]])).toBe(true)
    expect(testConditions([[null, () => false]])).toBe(false)
})

test('testConditions returns true when ALL conditions are true', () => {
    expect(testConditions([
        [true, isTruthy],
        [false, isFalsy],
        [Date.now(), isItself],
        ['foo', isFoo],
        [2, isHalfOfFour],
        [[6, 4], addToTen]
    ])).toBe(true)
})

test('testConditions returns false if ANY condition is false', () => {
    expect(testConditions([
        [null, () => false],
        [null, () => expect('testConditions').toBe('short-circuited already')]
    ])).toBe(false)
})