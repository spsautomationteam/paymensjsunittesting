import { Payment as mixinTest }  from '../../operations/payment'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'payment';
const fields = [ 'preAuth', 'orderNumber', 'totalAmount', 'taxAmount', 'shippingAmount', 'tipAmount', ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

