import { Vault as mixinTest }  from '../../operations/vault'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'vault';
const fields = [ 'operation', 'token' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

