import * as v from '../../validation/card';

const currentMonth = ((new Date()).getMonth() + 1).toString();
const currentYear = (new Date()).getFullYear().toString().slice(-2);

// getCardType:
test('getCardType accepts a string and returns the card type', () => {
    expect(v.getCardType('2')).toBe('MasterCard');
    expect(v.getCardType('3')).toBe('American Express');
    expect(v.getCardType('4')).toBe('Visa');
    expect(v.getCardType('5')).toBe('MasterCard');
    expect(v.getCardType('6')).toBe('Discover');
})

test('getCardType accepts a number and returns the card type', () => {
    expect(v.getCardType(3)).toBe('American Express');
    expect(v.getCardType(2)).toBe('MasterCard');
    expect(v.getCardType(4)).toBe('Visa');
    expect(v.getCardType(5)).toBe('MasterCard');
    expect(v.getCardType(6)).toBe('Discover');
})

test('getCardType defaults to returning an empty string', () => {
    expect(v.getCardType(null)).toBe('');
    expect(v.getCardType(void 0)).toBe('');
    expect(v.getCardType('')).toBe('');
    expect(v.getCardType(false as any)).toBe('');
    expect(v.getCardType(true as any)).toBe('');
    expect(v.getCardType('1')).toBe('');
    expect(v.getCardType(1)).toBe('');
    expect(v.getCardType(0)).toBe('');
    expect(v.getCardType('7')).toBe('');
    expect(v.getCardType(7)).toBe('');
    
})

// isAmex:
test('isAmex tests the first character for \'3\'', () => {
    expect(v.isAmex('321')).toBe(true);
    expect(v.isAmex('123')).toBe(false);
})

// isVisa:
test('isVisa tests the first character for \'4\'', () => {
    expect(v.isVisa('421')).toBe(true);
    expect(v.isVisa('123')).toBe(false);
})

// isMasterCard:
test('isMasterCard tests the first character for \'2\' or \'5\'', () => {
    expect(v.isMasterCard('521')).toBe(true);
    expect(v.isMasterCard('221')).toBe(true);
    expect(v.isMasterCard('123')).toBe(false);
})

// isDiscover:
test('isDiscover tests the first character for \'6\'', () => {
    expect(v.isDiscover('621')).toBe(true);
    expect(v.isDiscover('123')).toBe(false);
})

// isValidCardNumber:
test('isValidCardNumber is a curried function', () => {
    expect(typeof v.isValidCardNumber()).toBe('function');
})

test('isValidCardNumber\'s first argument can toggle card types off', () => {
    expect(v.isValidCardNumber()('371449635392376')).toBe(true);
    expect(v.isValidCardNumber({ amex: true })('371449635392376')).toBe(true);
    expect(v.isValidCardNumber({ amex: false })('371449635392376')).toBe(false);

    expect(v.isValidCardNumber()('4111111111111111')).toBe(true);
    expect(v.isValidCardNumber({ visa: true })('4111111111111111')).toBe(true);
    expect(v.isValidCardNumber({ visa: false })('4111111111111111')).toBe(false);

    expect(v.isValidCardNumber()('5454545454545454')).toBe(true);
    expect(v.isValidCardNumber({ mc: true })('5454545454545454')).toBe(true);
    expect(v.isValidCardNumber({ mc: false })('5454545454545454')).toBe(false);

    expect(v.isValidCardNumber()('6011000993026909')).toBe(true);
    expect(v.isValidCardNumber({ disc: true })('6011000993026909')).toBe(true);
    expect(v.isValidCardNumber({ disc: false })('6011000993026909')).toBe(false);
})

test('isValidCardNumber tests for numeric-only', () => {
    expect(v.isValidCardNumber()('4111111111111111')).toBe(true);
    expect(v.isValidCardNumber()('4111-1111-1111-1111')).toBe(false);
})

test('isValidCardNumber tests the length for 16, or 15 for Amex', () => {
    expect(v.isValidCardNumber()('4111111111111111')).toBe(true);
    expect(v.isValidCardNumber()('411111111111111')).toBe(false);
    expect(v.isValidCardNumber()('371449635392376')).toBe(true);
    expect(v.isValidCardNumber()('3714496353923701')).toBe(false)
})

test('isValidCardNumber does a mod10 check', () => {
    expect(v.isValidCardNumber()('4111111111111111')).toBe(true);
    expect(v.isValidCardNumber()('4111111111111110')).toBe(false);
})

// isAllowedCard
test('isAllowedCard\'s first argument can toggle card types off', () => {
    expect(v.isAllowedCard()('371449635392376')).toBe(true);
    expect(v.isAllowedCard({ amex: true })('371449635392376')).toBe(true);
    expect(v.isAllowedCard({ amex: false })('371449635392376')).toBe(false);

    expect(v.isAllowedCard()('4111111111111111')).toBe(true);
    expect(v.isAllowedCard({ visa: true })('4111111111111111')).toBe(true);
    expect(v.isAllowedCard({ visa: false })('4111111111111111')).toBe(false);

    expect(v.isAllowedCard()('5454545454545454')).toBe(true);
    expect(v.isAllowedCard({ mc: true })('5454545454545454')).toBe(true);
    expect(v.isAllowedCard({ mc: false })('5454545454545454')).toBe(false);

    expect(v.isAllowedCard()('6011000993026909')).toBe(true);
    expect(v.isAllowedCard({ disc: true })('6011000993026909')).toBe(true);
    expect(v.isAllowedCard({ disc: false })('6011000993026909')).toBe(false);
})

test('isAllowedCard does NOT test for numeric-only', () => {
    expect(v.isAllowedCard()('4111111111111111')).toBe(true);
    expect(v.isAllowedCard()('4111-1111-1111-1111')).toBe(true);
})

test('isAllowedCard does NOT test the length', () => {
    expect(v.isAllowedCard()('4111111111111111')).toBe(true);
    expect(v.isAllowedCard()('411111111111111')).toBe(true);
    expect(v.isAllowedCard()('371449635392376')).toBe(true);
    expect(v.isAllowedCard()('3714496353923701')).toBe(true)
})

test('isAllowedCard does NOT do a mod10 check', () => {
    expect(v.isAllowedCard()('4111111111111111')).toBe(true);
    expect(v.isAllowedCard()('4111111111111110')).toBe(true);
})

// isValidExpirationDate:
test('isValidExpirationDate tests for numeric-only', () => {
    expect(v.isValidExpirationDate(['12', '99'])).toBe(true);
    expect(v.isValidExpirationDate(['1a', '99'])).toBe(false);
    expect(v.isValidExpirationDate(['12', '9a'])).toBe(false);
    expect(v.isValidExpirationDate(['1a', '9a'])).toBe(false);
})

test('isValidExpirationDate tests month for length of 1, or 2 with appropriate start', () => {
    expect(v.isValidExpirationDate(['2', '99'])).toBe(true);
    expect(v.isValidExpirationDate(['02', '99'])).toBe(true);
    expect(v.isValidExpirationDate(['12', '99'])).toBe(true);
    expect(v.isValidExpirationDate(['123', '99'])).toBe(false);
    expect(v.isValidExpirationDate(['22', '99'])).toBe(false);
})

test('isValidExpirationDate tests month is a coherent month', () => {
    expect(v.isValidExpirationDate(['12', '99'])).toBe(true);
    expect(v.isValidExpirationDate(['13', '99'])).toBe(false);
    expect(v.isValidExpirationDate(['0', '99'])).toBe(false);
    expect(v.isValidExpirationDate(['00', '99'])).toBe(false);
})

test('isValidExpirationDate tests year for length of 2, or 4 with appropriate start', () => {
    expect(v.isValidExpirationDate(['12', '99'])).toBe(true);
    expect(v.isValidExpirationDate(['12', '2099'])).toBe(true);
    expect(v.isValidExpirationDate(['12', '9'])).toBe(false);
    expect(v.isValidExpirationDate(['12', '299'])).toBe(false);
    expect(v.isValidExpirationDate(['12', '209'])).toBe(false);
    expect(v.isValidExpirationDate(['12', '1999'])).toBe(false);
})

test('isValidExpirationDate tests year is current or future', () => {
    expect(v.isValidExpirationDate(['12', currentYear])).toBe(true);
    expect(v.isValidExpirationDate(['12', (parseInt(currentYear) + 1).toString()])).toBe(true);
    expect(v.isValidExpirationDate(['12', (parseInt(currentYear) - 1).toString()])).toBe(false);
})

test('isValidExpirationDate tests the month is current or future when the year is current', () => {
    expect(v.isValidExpirationDate([currentMonth, currentYear])).toBe(true);
    if (parseInt(currentMonth) !== 1) {
        expect(v.isValidExpirationDate([(parseInt(currentMonth) - 1).toString(), currentYear])).toBe(false);
    }
    if (parseInt(currentMonth) !== 12) {
        expect(v.isValidExpirationDate([(parseInt(currentMonth) + 1).toString(), currentYear])).toBe(true);
    }
})

// isValidCvv:
test('isValidCvv tests for numeric-only', () => {
    expect(v.isValidCvv('123')).toBe(true);
    expect(v.isValidCvv('12a')).toBe(false);
})

test('isValidCvv tests the length for 3 or 4', () => {
    expect(v.isValidCvv('12')).toBe(false);
    expect(v.isValidCvv('123')).toBe(true);
    expect(v.isValidCvv('1234')).toBe(true);
    expect(v.isValidCvv('12345')).toBe(false);
})