import * as v from '../../validation/ach';

// isValidRouting:
test('isValidRouting tests for numeric-only', () => {
    expect(v.isValidRouting('056008849')).toBe(true);
    expect(v.isValidRouting('05600884a')).toBe(false);
})

test('isValidRouting tests the length for 9', () => {
    expect(v.isValidRouting('056008849')).toBe(true);
    expect(v.isValidRouting('05600884')).toBe(false);
    expect(v.isValidRouting('0560088490')).toBe(false);
})

// isValidAccount:
test('isValidAccount tests for numeric-only', () => {
    expect(v.isValidAccount('999')).toBe(true);
    expect(v.isValidAccount('99a')).toBe(false);
})

// isValidType:
test('isValidType tests for \'checking\' or \'savings\'', () => {
    expect(v.isValidType('checking')).toBe(true);
    expect(v.isValidType('savings')).toBe(true);
    expect(v.isValidType('foo')).toBe(false);
})

test('isValidType is not case-sensitive', () => {
    expect(v.isValidType('checking')).toBe(true);
    expect(v.isValidType('savings')).toBe(true);
    expect(v.isValidType('foo')).toBe(false);
    expect(v.isValidType('Checking')).toBe(true);
    expect(v.isValidType('Savings')).toBe(true);
    expect(v.isValidType('Foo')).toBe(false);
    expect(v.isValidType('CHECKING')).toBe(true);
    expect(v.isValidType('SAVINGS')).toBe(true);
    expect(v.isValidType('FOO')).toBe(false);
    expect(v.isValidType('ChEcKiNg')).toBe(true);
    expect(v.isValidType('sAvInGs')).toBe(true);
    expect(v.isValidType('foo')).toBe(false);
})