import { parseCallback } from '../../api/response';

test('parseCallback is a function that returns an object', () => {
    expect(typeof parseCallback).toBe('function')
    expect(typeof parseCallback(null, null, null, null)).toBe('object')
})

test('parseCallback returns an object with the appropriate properties', () => {
    const r = parseCallback(null, null, null, null);
    expect(r.hasOwnProperty('err')).toBe(true);
    expect(r.hasOwnProperty('xhr')).toBe(true);
    expect(r.hasOwnProperty('data')).toBe(true);
    expect(r.hasOwnProperty('hash')).toBe(true);
    expect(r.hasOwnProperty('parsed')).toBe(true);
    expect(r.hasOwnProperty('processingResult')).toBe(true);
    expect(r.hasOwnProperty('avsResult')).toBe(true);
    expect(r.hasOwnProperty('cvvResult')).toBe(true);
})

test('parseCallback passes its parameters directly to the return object', () => {
    const r = parseCallback('foo', 'bar', 'baz', 'qum');
    expect(r.err).toBe('foo');
    expect(r.xhr).toBe('bar');
    expect(r.data).toBe('baz');
    expect(r.hash).toBe('qum');
})

test('parseCallback returns an object whose "parsed" property is the response data deserialized', () => {
    const fakeObj = { foo: 'bar' }
    const r = parseCallback(null, null, JSON.stringify(fakeObj), null);
    expect(r.parsed).toEqual(fakeObj);
})

test('parseCallback returns an object with the appropriate processingResult', () => {
    let r, d;
    
    // --- happy paths ---
    d = JSON.stringify({ gatewayResponse: { status: 'ApProVeD' }})
    r = parseCallback(false, null, d, null);
    expect(r.processingResult).toEqual('APPROVED');

    d = JSON.stringify({ gatewayResponse: { status: 'dEcLiNeD' }})
    r = parseCallback(false, null, d, null);
    expect(r.processingResult).toEqual('DECLINED');

    r = parseCallback(true, null, null, null); // <-- err
    expect(r.processingResult).toEqual('ERROR');

    d = JSON.stringify({ gatewayResponse: { vaultResponse: { status: 1 } }})
    r = parseCallback(false, null, d, null);
    expect(r.processingResult).toEqual('APPROVED');

    d = JSON.stringify({ gatewayResponse: { vaultResponse: { status: 0 } }})
    r = parseCallback(false, null, d, null);
    expect(r.processingResult).toEqual('ERROR');

    d = JSON.stringify({ vaultResponse: { status: 1 }})
    r = parseCallback(false, null, d, null);
    expect(r.processingResult).toEqual('APPROVED');

    d = JSON.stringify({ vaultResponse: { status: 0 } })
    r = parseCallback(false, null, d, null);
    expect(r.processingResult).toEqual('ERROR');

    // --- unhappy paths ---
    r = parseCallback(false, null, null, null); // <-- no data
    expect(r.processingResult).toEqual('ERROR');

    d = JSON.stringify({ foo: 'bar' });
    r = parseCallback(false, null, d, null); // <-- no data.gatewayResponse
    expect(r.processingResult).toEqual('ERROR');

    d = JSON.stringify({ gatewayResponse: { foo: 'bar' }});
    r = parseCallback(false, null, d, null);  // <-- no data.gatewayResponse.status/vaultResponse
    expect(r.processingResult).toEqual('ERROR');

    d = JSON.stringify({ gatewayResponse: { status: 'bar' }});
    r = parseCallback(false, null, d, null);  // <-- bad data.gatewayResponse.status
    expect(r.processingResult).toEqual('UNKNOWN');


    d = JSON.stringify({ gatewayResponse: { vaultResponse: 'bar' }});
    r = parseCallback(false, null, d, null);  // <-- bad data.gatewayResponse.vaultResponse
    expect(r.processingResult).toEqual('ERROR');
})

test('parseCallback returns an object with the appropriate avsResult', () => {
    let r, d;
    
    // --- happy paths ---
    ["X", "Y", "D", "J", "M", "Q", "V"].forEach(avsResult => {
        d = JSON.stringify({ gatewayResponse: { avsResult }})
        r = parseCallback(false, null, d, null);
        expect(r.avsResult).toEqual('EXACT');
    });

    ["W", "Z", "A", "B", "F", "H", "K", "L", "O", "P", "T"].forEach(avsResult => {
        d = JSON.stringify({ gatewayResponse: { avsResult }})
        r = parseCallback(false, null, d, null);
        expect(r.avsResult).toEqual('PARTIAL');
    });

    r = parseCallback(true, null, null, null); // <-- err
    expect(r.avsResult).toEqual('NONE');


    // --- unhappy paths ---

    [0, 1, true, false, void 0, {}].forEach(avsResult => {
        d = JSON.stringify({ gatewayResponse: { avsResult }})
        r = parseCallback(false, null, d, null);
        expect(r.avsResult).toEqual('NONE');
    });

    r = parseCallback(false, null, null, null); // <-- no data
    expect(r.avsResult).toEqual('NONE');

    d = JSON.stringify({ foo: 'bar' });
    r = parseCallback(false, null, d, null); // <-- no data.gatewayResponse
    expect(r.avsResult).toEqual('NONE');

    d = JSON.stringify({ gatewayResponse: { foo: 'bar' }});
    r = parseCallback(false, null, d, null);  // <-- no data.gatewayResponse.avsResult
    expect(r.avsResult).toEqual('NONE');

})

test('parseCallback returns an object with the appropriate cvvResult', () => {
    let r, d;
    
    // --- happy paths ---
    d = JSON.stringify({ gatewayResponse: { cvvResult: 'M' }})
    r = parseCallback(false, null, d, null);
    expect(r.cvvResult).toEqual('EXACT');

    ["N", "P", "U"].forEach(cvvResult => {
        d = JSON.stringify({ gatewayResponse: { cvvResult }})
        r = parseCallback(false, null, d, null);
        expect(r.cvvResult).toEqual('NONE');
    });

    r = parseCallback(true, null, null, null); // <-- err
    expect(r.cvvResult).toEqual('NONE');


    // --- unhappy paths ---
    [0, 1, true, false, void 0, {}].forEach(cvvResult => {
        d = JSON.stringify({ gatewayResponse: { cvvResult }})
        r = parseCallback(false, null, d, null);
        expect(r.cvvResult).toEqual('NONE');
    });

    r = parseCallback(false, null, null, null); // <-- no data
    expect(r.cvvResult).toEqual('NONE');

    d = JSON.stringify({ foo: 'bar' });
    r = parseCallback(false, null, d, null); // <-- no data.gatewayResponse
    expect(r.cvvResult).toEqual('NONE');

    d = JSON.stringify({ gatewayResponse: { foo: 'bar' }});
    r = parseCallback(false, null, d, null);  // <-- no data.gatewayResponse.cvvResult
    expect(r.cvvResult).toEqual('NONE');

})