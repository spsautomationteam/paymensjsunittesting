import { parseCallback as original } from '../../api/response'
import { parseCallback as reexport } from '../../api/request'

test('request reexports parseCallback from response', () => {
    expect(reexport).toBe(original);
})