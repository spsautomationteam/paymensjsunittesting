import { Level3 as mixinTest }  from '../../features/level3'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'level3';

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        [ 'customerNumber', 'destinationCountryCode' ],
        { 
            'amounts': [ 'discount', 'duty', 'nationalTax' ],
            'vat': [ 'idNumber', 'invoiceNumber', 'amount', 'rate' ]
        }
    )
})

