import { Config as mixinTest }  from '../../features/config'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'Config';
const fields = [ 'allowPartialAuthorization', 'cardPresent', 'authorizationCode', 'deviceId', 'terminalNumber', 'serialNumber', 'kernelVersion' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

