import * as allMixins  from '../../features/all'
const mixins = ['Bank', 'Billing', 'Config', 'Custom', 'Customer', 'Debit', 'FSA', 'Level2', 'Level3', 'Recurring', 'Shipping'];
const weirdMixins = [ ['postback', 'Postback' ] ] // bah

test('features/all exports all features', () => {
    mixins.forEach(f => {
        expect(allMixins.hasOwnProperty(f)).toBe(true);
        expect(typeof allMixins[f]).toBe('object');
        expect(allMixins[f].name).toBe(f.toLowerCase());
    });
    weirdMixins.forEach(wf => {
        expect(allMixins.hasOwnProperty(wf[1])).toBe(true);
        expect(typeof allMixins[wf[1]]).toBe('object');
        expect(allMixins[wf[1]].name).toBe(wf[0]);
    });
    expect(Object.getOwnPropertyNames(allMixins).length)
        .toBe(mixins.length + weirdMixins.length + 1); // <-- ts adds __esModule
})