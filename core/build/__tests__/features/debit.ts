import { Debit as mixinTest }  from '../../features/debit'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'Debit';
const fields = [ 'UNRELEASED' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

