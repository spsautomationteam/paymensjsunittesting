import { Bank as mixinTest }  from '../../features/bank'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'Bank';
const fields = [ 'retrieve' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})
