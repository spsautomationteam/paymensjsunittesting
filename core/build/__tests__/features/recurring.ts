import { Recurring as mixinTest }  from '../../features/recurring'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'Recurring';
const fields = [ 'isRecurring', 'recurringSchedule' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        [ 'isRecurring' ],
        { 'recurringSchedule': ['amount', 'frequency', 'interval', 'nonBusinessDaysHandling', 'startDate', 'totalCount', 'groupId'] }
    )
})

