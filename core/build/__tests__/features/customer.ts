import { Customer as mixinTest }  from '../../features/customer'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'Customer';
const fields = [ 'email', 'telephone', 'fax', 'ein', 'ssn', 'dateOfBirth', 'license' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

