import { Level2 as mixinTest }  from '../../features/level2'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'level2';
const fields = [ 'customerNumber' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

