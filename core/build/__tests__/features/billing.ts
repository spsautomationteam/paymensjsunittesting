import { Billing as mixinTest } from '../../features/billing'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'billing';
const fields = [ 'name', 'address', 'city', 'state', 'postalCode', 'country' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})
