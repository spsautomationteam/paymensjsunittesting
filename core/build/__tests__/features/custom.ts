import { Custom as mixinTest }  from '../../features/custom'
const mixinName = 'custom';

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    expect(mixinTest.data).toBeUndefined();
})

