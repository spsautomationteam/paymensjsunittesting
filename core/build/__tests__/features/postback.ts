import { Postback as mixinTest }  from '../../features/postback'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'postback';
const fields = [ 'url', 'callback' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName);
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

