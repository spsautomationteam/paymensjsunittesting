import { FSA as mixinTest } from '../../features/fsa'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'fsa';
const fields = [ 'iiasVerification', 'amounts' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        ['iiasVerification'],
        { 
            'amounts': ['healthCare', 'prescription', 'clinic', 'dental', 'vision']
        }
    )
})

