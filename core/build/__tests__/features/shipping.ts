import { Shipping as mixinTest }  from '../../features/shipping'
import { testMixinProps } from '../_helpers/testMixinProps'
const mixinName = 'Shipping';
const fields = [ 'name', 'address', 'city', 'state', 'postalCode', 'country' ];

test('mixin has the appropriate name', () => {
    expect(mixinTest.name).toBe(mixinName.toLowerCase());
})

test('mixin has the appropriate fields', () => {
    testMixinProps(
        mixinTest.data,
        fields,
        {}
    )
})

