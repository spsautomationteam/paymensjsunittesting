import { isVaultOnlyRequest, isPaymentRequest } from '../../utility/operations';

test('isVaultOnlyRequest returns a boolean', () => {
    expect(typeof isVaultOnlyRequest()).toBe('boolean');
})

test('isVaultOnlyRequest determines whether a request\'s primary operation is "vault"', () => {
    expect(isVaultOnlyRequest()).toBe(false);
    expect(isVaultOnlyRequest([ { name: 'payment', data: {} }, { name: 'vault', data: {} }, { name: 'foo', data: {} } ])).toBe(false);
    expect(isVaultOnlyRequest([ { name: 'vault', data: {} }, { name: 'payment', data: {} }, { name: 'foo', data: {} } ])).toBe(true);
})

test('isVaultOnlyRequest calls asArray on its argument', () => {
    expect(isVaultOnlyRequest()).toBe(false);
    expect(isVaultOnlyRequest({ name: 'payment', data: {} })).toBe(false);
    expect(isVaultOnlyRequest({ name: 'vault', data: {} })).toBe(true);
})

test('isPaymentRequest returns a boolean', () => {
    expect(typeof isPaymentRequest()).toBe('boolean');
})

test('isPaymentRequest determines whether a request\'s primary operation is "payment"', () => {
    expect(isPaymentRequest()).toBe(false);
    expect(isPaymentRequest([ { name: 'payment', data: {} }, { name: 'vault', data: {} }, { name: 'foo', data: {} } ])).toBe(true);
    expect(isPaymentRequest([ { name: 'vault', data: {} }, { name: 'payment', data: {} }, { name: 'foo', data: {} } ])).toBe(false);
})

test('isPaymentRequest calls asArray on its argument', () => {
    expect(isPaymentRequest()).toBe(false);
    expect(isPaymentRequest({ name: 'payment', data: {} })).toBe(true);
    expect(isPaymentRequest({ name: 'vault', data: {} })).toBe(false);
})

