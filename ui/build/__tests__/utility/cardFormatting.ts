import * as f from '../../utility/cardFormatting';

// formatCardNumber:

test('formatCardNumber correctly formats 15- and 16-digit numbers', () => {
    expect(f.formatCardNumber()('371449635392376')).toBe('3714-496353-92376'));
    expect(f.formatCardNumber()('4111111111111111')).toBe('4111-1111-1111-1111'));
    expect(f.formatCardNumber()('5454545454545454')).toBe('5454-5454-5454-5454'));
    expect(f.formatCardNumber()('6011000993026909')).toBe('6011-0009-9302-6909'));
})

test('formatCardNumber accepts an optional delimiter', () => {
    expect(f.formatCardNumber('/')('371449635392376')).toBe('3714/496353/92376'));
    expect(f.formatCardNumber('~')('4111111111111111')).toBe('4111~1111~1111~1111'));
    expect(f.formatCardNumber(' ')('5454545454545454')).toBe('5454 5454 5454 5454'));
    expect(f.formatCardNumber('')('6011000993026909')).toBe('6011000993026909'));
})

test('formatCardNumber should accept the number as a number', () => {
    expect(f.formatCardNumber()(371449635392376)).toBe('3714-496353-92376'));
    expect(f.formatCardNumber()(4111111111111111)).toBe('4111-1111-1111-1111'));
    expect(f.formatCardNumber()(5454545454545454)).toBe('5454-5454-5454-5454'));
    expect(f.formatCardNumber()(6011000993026909)).toBe('6011-0009-9302-6909'));
    expect(f.formatCardNumber('/')(371449635392376)).toBe('3714/496353/92376'));
    expect(f.formatCardNumber('~')(4111111111111111)).toBe('4111~1111~1111~1111'));
    expect(f.formatCardNumber(' ')(5454545454545454)).toBe('5454 5454 5454 5454'));
    expect(f.formatCardNumber('')(6011000993026909)).toBe('6011000993026909'));
})

test('formatCardNumber blobs everything after 15/16 characters', () => {
    expect(f.formatCardNumber()('3714496353923769999')).toBe('3714-496353-92376-9999'));
    expect(f.formatCardNumber()('41111111111111119999')).toBe('4111-1111-1111-1111-9999'));
    expect(f.formatCardNumber()('54545454545454549999')).toBe('5454-5454-5454-5454-9999'));
    expect(f.formatCardNumber()('60110009930269099999')).toBe('6011-0009-9302-6909-9999'));
    // this doesn't work with numbers! they're too long; rounding gets weird
})

test('formatCardNumber removes non-numeric characters before formatting', () => {
    expect(f.formatCardNumber()('371a449b635392c376')).toBe('3714-496353-92376'));
    expect(f.formatCardNumber()('41111!111111 11111')).toBe('4111-1111-1111-1111'));
    expect(f.formatCardNumber()('54545__45454545454')).toBe('5454-5454-5454-5454'));
    expect(f.formatCardNumber()('60110009930\n26909        ')).toBe('6011-0009-9302-6909'));
})

// formatExpirationDate:
test('formatExpirationDate inserts a slash after the second character', () => {
    expect(f.formatExpirationDate()('1215')).toBe('12/15'));
})

test('formatExpirationDate accepts an optional delimiter', () => {
    expect(f.formatExpirationDate('~')('1215')).toBe('12~15'));
})

// maskCardNumber:

test('maskCardNumber correctly masks 15- and 16-digit numbers', () => {
    expect(f.maskCardNumber()('371449635392376')).toBe('371449XXXXX2376'));
    expect(f.maskCardNumber()('4111111111111111')).toBe('411111XXXXXX1111'));
    expect(f.maskCardNumber()('5454545454545454')).toBe('545454XXXXXX5454'));
    expect(f.maskCardNumber()('6011000993026909')).toBe('601100XXXXXX6909'));
})

test('maskCardNumber accepts an optional masking character', () => {
    expect(f.maskCardNumber('*')('371449635392376')).toBe('371449*****2376'));
    expect(f.maskCardNumber(' ')('4111111111111111')).toBe('411111      1111'));
    expect(f.maskCardNumber('@')('5454545454545454')).toBe('545454@@@@@@5454'));
    expect(f.maskCardNumber('..')('6011000993026909')).toBe('601100............6909'));
})

test('maskCardNumber should accept the number as a number', () => {
    expect(f.maskCardNumber()(371449635392376)).toBe('371449XXXXX2376'));
    expect(f.maskCardNumber()(4111111111111111)).toBe('411111XXXXXX1111'));
    expect(f.maskCardNumber()(5454545454545454)).toBe('545454XXXXXX5454'));
    expect(f.maskCardNumber()(6011000993026909)).toBe('601100XXXXXX6909'));
    expect(f.maskCardNumber('*')(371449635392376)).toBe('371449*****2376'));
    expect(f.maskCardNumber(' ')(4111111111111111)).toBe('411111      1111'));
    expect(f.maskCardNumber('@')(5454545454545454)).toBe('545454@@@@@@5454'));
    expect(f.maskCardNumber('..')(6011000993026909)).toBe('601100............6909'));
})

test('maskCardNumber removes non-numeric characters before formatting', () => {
    expect(f.maskCardNumber()('3714a496b3539c2376')).toBe('371449XXXXX2376'));
    expect(f.maskCardNumber()('411111!1111?11/1111')).toBe('411111XXXXXX1111'));
    expect(f.maskCardNumber()('5454 5454 5454 5454')).toBe('545454XXXXXX5454'));
    expect(f.maskCardNumber()('601100099\n3026909')).toBe('601100XXXXXX6909'));
})

// Added New Test Cases
test('SpyOn on formatCardNumber function', () => {
  const spy = jest.spyOn(f, 'formatCardNumber');
  const cfn = f.formatCardNumber();
  expect(spy).toHaveBeenCalled();
  expect(cfn).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('spyon maskCardNumber function', () => {
  const spy = jest.spyOn(f, 'maskCardNumber');
  const mcn = f.maskCardNumber();
  expect(spy).toHaveBeenCalled();
  expect(mcn).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('spyon formatExpirationDate function', () => {
  const spy = jest.spyOn(f, 'formatExpirationDate');
  const fed = f.formatExpirationDate();
  expect(spy).toHaveBeenCalled();
  expect(fed).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('cardFormatting module exports a object', () => {
    expect(typeof f).toBe('object');
})

test('cardFormatting module returns 3 functions', () => { 
  expect(typeof f.formatCardNumber()).toBe('function');
  expect(typeof f.maskCardNumber()).toBe('function');
  expect(typeof f.formatExpirationDate()).toBe('function');
  
})

test('formatCardNumber by removing non-numeric characters with optional delimiter', () => {    
	expect(f.formatCardNumber('')('371a449b635392c376')).toBe('371449635392376'));
    expect(f.formatCardNumber('')('4111-1111_1111a1111')).toBe('4111111111111111'));
    expect(f.formatCardNumber('')('54545__45454545454')).toBe('5454545454545454'));
    expect(f.formatCardNumber('')('60110009930\n26909        ')).toBe('6011000993026909'));	
})

test('formatCardNumber correctly formats 15- and 16-digit numbers with appropriate size', () => {
    expect(f.formatCardNumber()('371449635392376').length).toBe(17));
	 expect(f.formatCardNumber()('371449635392376').length).not.toBeGreaterThan(17));
	  expect(f.formatCardNumber()('371449635392376').length).not.toBeLessThan(17));
	
    expect(f.formatCardNumber()('4111111111111111').length).toBe(19);
	 expect(f.formatCardNumber()('4111111111111111').length).not.toBeGreaterThan(19);
		expect(f.formatCardNumber()('4111111111111111').length).not.toBeLessThan(19);
	
	
    expect(f.formatCardNumber()('5454545454545454').length).toBe(19);
	 expect(f.formatCardNumber()('5454545454545454').length).not.toBeGreaterThan(19);
		expect(f.formatCardNumber()('5454545454545454').length).not.toBeLessThan(19);	
	
    expect(f.formatCardNumber()('6011000993026909').length).toBe(19);
	 expect(f.formatCardNumber()('6011000993026909').length).not.toBeGreaterThan(19);
		expect(f.formatCardNumber()('6011000993026909').length).not.toBeLessThan(19);
	
	
})

test('formatCardNumber accepts an optional delimiter with appropriate size', () => {
    expect(f.formatCardNumber('/')('371449635392376').length).toBe(17);
    expect(f.formatCardNumber('~')('4111111111111111').length).toBe(19);
    expect(f.formatCardNumber(' ')('5454545454545454').length).toBe(19);
    expect(f.formatCardNumber('')('6011000993026909').length).toBe(16);
})

test('cardFormatting module exports 3 functions with single argument', () => {
    expect(f.formatCardNumber().length).toBe(1);
	expect(f.maskCardNumber().length).toBe(1);
	expect(f.formatExpirationDate().length).toBe(1);
   
})

test('formatCardNumber returns 15- and 16-digit card number and it should be in string format', () => {
    expect(typeof f.formatCardNumber()('371449635392376')).toBe('string');
    expect(typeof f.formatCardNumber()('4111111111111111')).toBe('string');
    expect(typeof f.formatCardNumber()('5454545454545454')).toBe('string');
    expect(typeof f.formatCardNumber()('6011000993026909')).toBe('string');
})

test('formatCardNumber function validation for Amex card when passing delimiter', () => {
    expect(f.formatCardNumber('-')('3999999999')).toBe('3999-999999'));
	expect(f.formatCardNumber('-')('3999999999')).not.toBe('3999-9999-99'));	
    
})

test('formatCardNumber returns 15- and 16-digit card number along with specified delimiter', () => {
    expect(f.formatCardNumber()('371449635392376')).toContain('-'));
	expect(f.formatCardNumber()('4111111111111111')).toContain('-'));
    expect(f.formatCardNumber()('5454545454545454')).toContain('-'));
    expect(f.formatCardNumber()('6011000993026909')).toContain('-'));
	
   expect(f.formatCardNumber('/')('371449635392376')).toContain('/'));
    expect(f.formatCardNumber('$')('4111111111111111')).toContain('$'));
    expect(f.formatCardNumber(' ')('5454545454545454')).toContain(' '));
    expect(f.formatCardNumber('')('6011000993026909')).toContain(''));
})

// new test cases for formatExpirationDate:
test('Verify formatExpirationDate function validations', () => {
	
	expect(f.formatExpirationDate()('1215')).toBe('12/15');
	expect(f.formatExpirationDate()('1215')).toContain('/');
	
	expect(f.formatExpirationDate()('1215').length).toBe(5);
	expect(f.formatExpirationDate()('1215').length).not.toBeGreaterThan(5);
	expect(f.formatExpirationDate()('1215').length).not.toBeLessThan(5);
	
	expect(typeof f.formatExpirationDate()('1215')).toBe('string');
	
	expect(f.formatExpirationDate('~')('1215')).toBe('12~15'));
	expect(f.formatExpirationDate('~')('1215')).toContain('~'));
	 
	expect(f.formatExpirationDate('~')('1215').length).toBe(5);
	expect(f.formatExpirationDate('~')('1215').length).not.toBeGreaterThan(5);
	expect(f.formatExpirationDate('~')('1215').length).not.toBeLessThan(5);	
	
	expect(typeof f.formatExpirationDate('~')('1215')).toBe('string');
})

test('Verify formatExpirationDate function with other validations', () => {
	expect(f.formatExpirationDate()('')).toBe('');
	expect(f.formatExpirationDate()('$#$@#')).toBe('');
	expect(f.formatExpirationDate()('abcd')).toBe('');
	
    expect(f.formatCardNumber('/')('1215')).toBe('1215'));
    expect(f.formatCardNumber('@')('1215')).toBe('1215'));
    expect(f.formatCardNumber(' ')('1215')).toBe('1215'));
    expect(f.formatCardNumber('')('1215')).toBe('1215'));
})

test('Verify formatExpirationDate function with expire date as less than 4 digits', () => {
	expect(f.formatExpirationDate()('315')).toBe('31/5');
	expect(f.formatExpirationDate()('315').length).toBe(4);
	expect(f.formatExpirationDate()('315').length).not.toBeGreaterThan(4);
	expect(f.formatExpirationDate()('315').length).not.toBeLessThan(4);	
	expect(typeof f.formatExpirationDate()('315')).toBe('string');
	
})

test('Verify formatExpirationDate function with expire date as more than 4 digits', () => {
	expect(f.formatExpirationDate()('12155')).toBe('12/155');
	expect(f.formatExpirationDate()('12155').length).toBe(6);
	expect(f.formatExpirationDate()('12155').length).not.toBeGreaterThan(6);
	expect(f.formatExpirationDate()('12155').length).not.toBeLessThan(6);	
	expect(typeof f.formatExpirationDate()('12155')).toBe('string');
	
})

test('maskCardNumber correctly masks 15- and 16-digit numbers with exact mask character size', () => {
	
	expect(f.maskCardNumber()('3714a496b3539c2376')).toContain('XXXXX'));    //  should contain 5 'X' mask characters
	expect(f.maskCardNumber()('3714a496b3539c2376')).not.toContain('XXXXXX'));    
	
    expect(f.maskCardNumber()('4111111111111111')).toContain('XXXXXX');  //  should contain 6 'X' mask characters
	expect(f.maskCardNumber()('4111111111111111')).not.toContain('XXXXXXX');
	
    expect(f.maskCardNumber()('5454545454545454')).toContain('XXXXXX'); //  should contain 6 'X' mask characters
	expect(f.maskCardNumber()('5454545454545454')).not.toContain('XXXXXXX');
	
    expect(f.maskCardNumber()('6011000993026909')).toContain('XXXXXX'); //  should contain 6 'X' mask characters
	expect(f.maskCardNumber()('6011000993026909')).not.toContain('XXXXXXX');
})

test('maskCardNumber correctly masks 15- and 16-digit numbers by matching exact cardnumber length', () => {
    expect(f.maskCardNumber()('371449635392376')).toHaveLength(15);
    expect(f.maskCardNumber()('4111111111111111')).toHaveLength(16);
    expect(f.maskCardNumber()('5454545454545454')).toHaveLength(16);
    expect(f.maskCardNumber()('6011000993026909')).toHaveLength(16);
})

test('maskCardNumber accepts an optional masking character with exact mask character size', () => {
	expect(f.maskCardNumber('*')('3714a496b3539c2376')).toContain('*****')); // * size should be 5
	expect(f.maskCardNumber('*')('3714a496b3539c2376')).not.toContain('******'));  // * size not more than 5
	
    expect(f.maskCardNumber(' ')('4111111111111111')).toContain('      ')); // size should be 6
	expect(f.maskCardNumber(' ')('4111111111111111')).not.toContain('       '));	
	
    expect(f.maskCardNumber('@')('5454545454545454')).toContain('@@@@@@')); // @ size should be 6
	expect(f.maskCardNumber('@')('5454545454545454')).not.toContain('@@@@@@@'));	
	
    expect(f.maskCardNumber('..')('6011000993026909')).toContain('............'));  // .. size should be 6
	 expect(f.maskCardNumber('..')('6011000993026909')).not.toContain('..............'));
	
})

test('maskCardNumber removes non-numeric characters and return card number in specified length', () => {
    expect(f.maskCardNumber()('3714a496b3539c2376')).toHaveLength(15);
    expect(f.maskCardNumber()('411111!1111?11/1111')).toHaveLength(16);
    expect(f.maskCardNumber()('5454 5454 5454 5454')).toHaveLength(16);
    expect(f.maskCardNumber()('601100099\n3026909')).toHaveLength(16);
})





