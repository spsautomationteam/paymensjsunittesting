"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
class PaymentMethod extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange() {
        //handleChange(e : React.SyntheticEvent<HTMLAnchorElement>) {
        this.props.updater(this.props.method);
    }
    render() {
        return (React.createElement("span", { className: 'sps-payment-select-option', onClick: this.handleChange }, !!this.props.imgTag
            ? React.createElement("i", { className: `sps-method-sprite sps-method-sprite-${this.props.imgTag}-${this.props.isActive && 'selected' || 'default'}` })
            : React.createElement("span", { className: this.props.isActive ? 'active' : 'inactive' }, this.props.text)));
    }
}
exports.PaymentMethod = PaymentMethod;
