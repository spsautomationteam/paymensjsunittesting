"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var regex_1 = require("@payjs/core/utility/regex");
class KountFrame extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        const kountUrl = `https://qa.sagepayments.net/web_services/Frank/images/${regex_1.stripNonAlphaNumeric(this.props.salt).slice(0, 20)}?GatewayId=${this.props.merchantId}&Source=PAYJS`;
        return (React.createElement("iframe", { width: "0", height: "0", src: kountUrl, scrolling: "no", style: { borderWidth: 0 } },
            React.createElement("img", { width: "0", height: "0", src: kountUrl })));
    }
}
exports.KountFrame = KountFrame;
