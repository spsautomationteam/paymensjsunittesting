/// <reference types="react" />
import React = require('react');
export declare class CvvHelp extends React.PureComponent<any, any> {
    constructor(props: any);
    getHelpText(type: string): JSX.Element;
    render(): JSX.Element;
}
