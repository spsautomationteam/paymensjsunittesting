"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var card_1 = require("@payjs/core/validation/card");
class CardType extends React.PureComponent {
    constructor(props) {
        super(props);
        this.getIconClass = this.getIconClass.bind(this);
    }
    getIconClass(s) {
        let ret = '';
        switch (card_1.getCardType(s)) {
            case 'Visa':
                ret = 'V';
                break;
            case 'MasterCard':
                ret = 'MC';
                break;
            case 'Discover':
                ret = 'D';
                break;
            case 'American Express':
                ret = 'AX';
                break;
        }
        return ret;
    }
    render() {
        return (!!this.props.type
            ? React.createElement("span", { className: `sps sps-card-logo sps-cardtype-sprite sps-cardtype-sprite-${this.getIconClass(this.props.type)}-enabled` })
            : (React.createElement("span", null,
                this.props.allowedCards.visa && React.createElement("span", { className: `sps sps-card-logo sps-cardtype-sprite sps-cardtype-sprite-${this.getIconClass('4')}-disabled` }),
                this.props.allowedCards.mc && React.createElement("span", { className: `sps sps-card-logo sps-cardtype-sprite sps-cardtype-sprite-${this.getIconClass('5')}-disabled` }),
                this.props.allowedCards.amex && React.createElement("span", { className: `sps sps-card-logo sps-cardtype-sprite sps-cardtype-sprite-${this.getIconClass('3')}-disabled` }),
                this.props.allowedCards.disc && React.createElement("span", { className: `sps sps-card-logo sps-cardtype-sprite sps-cardtype-sprite-${this.getIconClass('6')}-disabled` }))));
    }
}
exports.CardType = CardType;
