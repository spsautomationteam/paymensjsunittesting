"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
class ProcessingResult extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("div", { className: "sps text-center result-div" },
            React.createElement("strong", { style: { fontSize: '20px' } }, this.props.processingResult),
            React.createElement("br", null),
            React.createElement("span", { style: { fontSize: '17px' } }, (() => {
                switch (this.props.processingResult) {
                    case 'APPROVED':
                        return 'This is approval text.';
                    case 'DECLINED':
                        return 'This is decline text.';
                    default:
                        return 'This is error text.';
                }
            })()),
            React.createElement("br", null),
            React.createElement("br", null),
            React.createElement("span", { style: { fontSize: '100px' }, className: `sps glyphicon glyphicon-${(() => {
                    switch (this.props.processingResult) {
                        case 'APPROVED':
                            return 'ok-circle'; //'confirm'
                        case 'DECLINED':
                            return 'alert';
                        default:
                            return 'question-sign'; //'help';
                    }
                })()}`, "aria-hidden": "true" })));
    }
}
exports.ProcessingResult = ProcessingResult;
