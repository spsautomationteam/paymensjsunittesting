"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
class LoadingBar extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("img", { className: "footer-element", src: "https://qa.sagepayments.net/pay/ui/0.3.11/css/assets/img/SignIn_Progress-Looping.gif" }));
    }
}
exports.LoadingBar = LoadingBar;
