/// <reference types="react" />
import React = require('react');
import { errorProps } from '../../interfaces/shared';
export declare class Error extends React.PureComponent<errorProps, any> {
    constructor(props: errorProps);
    render(): JSX.Element;
}
