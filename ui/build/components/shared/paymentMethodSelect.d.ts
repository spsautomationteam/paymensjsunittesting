/// <reference types="react" />
import React = require('react');
import { paymentMethodSelectProps } from "../../interfaces/paymentMethod";
export declare class PaymentMethodSelect extends React.PureComponent<paymentMethodSelectProps, any> {
    constructor(props: paymentMethodSelectProps);
    render(): JSX.Element;
}
