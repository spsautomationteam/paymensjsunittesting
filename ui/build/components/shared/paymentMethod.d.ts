/// <reference types="react" />
import React = require('react');
import { paymentMethodProps } from "../../interfaces/paymentMethod";
export declare class PaymentMethod extends React.PureComponent<paymentMethodProps, any> {
    constructor(props: paymentMethodProps);
    handleChange(): void;
    render(): JSX.Element;
}
