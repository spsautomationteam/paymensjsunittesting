"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var bootstrap_1 = require("./bootstrap");
var card_1 = require("@payjs/core/validation/card");
class Cvv extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("span", { className: 'sps sps-cvv-help-section', id: 'sps-cvv-help-section' },
            React.createElement("div", { className: 'sps sps-sample-cc' },
                React.createElement("span", { className: 'sps sps-sample-magstrip' }),
                React.createElement("span", { className: 'sps sps-sample-cvv' }, "123")),
            React.createElement("p", null,
                React.createElement("strong", { className: 'sps' }, this.props.type),
                React.createElement("br", null),
                "Back of card, 3 digits")));
    }
}
class Cid extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("span", { className: 'sps sps-cid-help-section', id: 'sps-cid-help-section' },
            React.createElement("div", { className: 'sps sps-sample-cc' },
                React.createElement("span", { className: 'sps sps-sample-cid' }, "1234"),
                React.createElement("p", { className: 'sps sps-sample-cc-number text-center' }, "3714 496353 92376")),
            React.createElement("p", null,
                React.createElement("strong", { className: 'sps' }, "American Express"),
                React.createElement("br", null),
                "Front of card, 4 digits")));
    }
}
class CvvHelp extends React.PureComponent {
    constructor(props) {
        super(props);
        this.getHelpText = this.getHelpText.bind(this);
    }
    getHelpText(type) {
        return (!type && React.createElement("span", null,
            React.createElement(Cvv, { type: 'Visa/MasterCard/Discover' }),
            React.createElement(Cid, null)))
            || card_1.isAmex(type) && React.createElement(Cid, null)
            || React.createElement(Cvv, { type: card_1.getCardType(type) });
    }
    render() {
        return (React.createElement("span", null,
            React.createElement("span", { className: 'pull-left' }, "Security (or CVV) Code"),
            React.createElement("span", { className: 'pull-right' },
                React.createElement(bootstrap_1.OverlayTrigger, { placement: "left", overlay: React.createElement(bootstrap_1.Tooltip, { id: 'sps-cvvTooltip' }, this.getHelpText(this.props.type)) },
                    React.createElement("span", { className: 'sps icon-help' })))));
    }
}
exports.CvvHelp = CvvHelp;
