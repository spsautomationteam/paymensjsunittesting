"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var methodFormMaps_1 = require("../../utility/methodFormMaps");
class PaymentMethodSelect extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("div", { className: "sps-payment-select" },
            React.createElement("div", { className: "sps-payment-select-caption" }, (this.props.wasSubmitted && 'Processing...') || 'Choose your payment method'),
            React.createElement("div", { className: "sps-payment-select-options" }, this.props.formMethodState.methods.map(m => m.name !== 'device' && methodFormMaps_1.getPaymentMethodSelect(this.props.formMethodState, m, this.props.formMethodState.isVaultRequest)))));
    }
}
exports.PaymentMethodSelect = PaymentMethodSelect;
