"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
class Error extends React.PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return React.createElement("div", { className: "sps sps-hide bubble form-validation error" }, this.props.message);
    }
}
exports.Error = Error;
