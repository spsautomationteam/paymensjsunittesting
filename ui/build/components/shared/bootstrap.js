"use strict";
// import, tweak, and reexport components from react-bootstrap.
// this lets us make ad hoc updates.
Object.defineProperty(exports, "__esModule", { value: true });
var react_bootstrap_1 = require("react-bootstrap");
const overridenMethod = react_bootstrap_1.Modal.prototype.updateStyle;
react_bootstrap_1.Modal.prototype.updateStyle = function updateStyle() {
    try {
        overridenMethod();
    }
    catch (ex) {
        // this likes to throw errors >_>
    }
};
exports.Modal = react_bootstrap_1.Modal;
exports.OverlayTrigger = react_bootstrap_1.OverlayTrigger;
exports.Tooltip = react_bootstrap_1.Tooltip;
