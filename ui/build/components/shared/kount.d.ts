/// <reference types="react" />
import React = require('react');
import { kountProps } from '../../interfaces/shared';
export declare class KountFrame extends React.PureComponent<kountProps, any> {
    constructor(props: kountProps);
    render(): JSX.Element;
}
