/// <reference types="react-bootstrap" />
/// <reference types="react" />
export declare const Modal: ReactBootstrap.ModalClass;
export declare const OverlayTrigger: React.ClassicComponentClass<ReactBootstrap.OverlayTriggerProps>;
export declare const Tooltip: React.ClassicComponentClass<ReactBootstrap.TooltipProps>;
