/// <reference types="react" />
import React = require('react');
import { cardTypeProps } from '../../interfaces/shared';
export declare class CardType extends React.PureComponent<cardTypeProps, any> {
    constructor(props: any);
    getIconClass(s: string): string;
    render(): JSX.Element;
}
