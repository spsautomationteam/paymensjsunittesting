"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var text_1 = require("../fields/text");
var strings_1 = require("@payjs/core/utility/strings");
class BillingForm extends React.Component {
    constructor(props) {
        super(props);
        const value = this.props.feature.data;
        const valid = (() => {
            const ret = {};
            Object.keys(value).map(key => ret[key] = value[key] && value[key].length > 0);
            return ret;
        })(); // <--- iife
        this.state = { value, valid };
        this.updateFormState = this.updateFormState.bind(this);
        this.checkValidityState = this.checkValidityState.bind(this);
    }
    componentDidMount() {
        this.checkValidityState();
    }
    updateFormState(field) {
        return ((update) => {
            const ret = this.state;
            ret['value'][field] = update[0];
            ret['valid'][field] = update[1];
            this.setState(ret);
            this.checkValidityState();
        });
    }
    checkValidityState() {
        const allValid = Object.keys(this.state.valid).every(f => !!this.state.valid[f] || (this.state.valid[f] === void 0 && this.props.hide.indexOf(f) !== -1));
        this.props.feature.data = this.state.value; // bad!
        this.props.submittableCallback([allValid, this.state.value]);
    }
    render() {
        return (React.createElement("div", { className: "sps sps-form" },
            React.createElement("div", { style: { minHeight: '45px' } },
                React.createElement("div", { className: "sps-form-caption pull-left" }, "Billing Info")),
            Object.keys(this.props.feature.data).map(k => this.props.hide.indexOf(k) > -1 ? null :
                (React.createElement(text_1.Text, { value: this.props.feature.data[k], key: k, name: k, label: strings_1.camelCaseToTitle(k), disabled: this.props.disable.indexOf(k) > -1 || this.props.isProcessing || this.props.ignoreFeature, placeholder: "", formatter: s => s, validator: s => !!s && s.length > 0, constrainer: () => true, isValid: this.state.valid[k], callback: this.updateFormState(k) })))));
    }
}
exports.BillingForm = BillingForm;
