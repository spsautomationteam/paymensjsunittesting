"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var http_1 = require("@payjs/core/utility/http");
var arrays_1 = require("@payjs/core/utility/arrays");
class MasterpassForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasRequestToken: false,
            hasVendorLib: false,
            wasSuccess: false,
            wasCancelled: false,
            wasFailure: false
        };
        this.componentDidMount = this.componentDidMount.bind(this);
        this.componentWillUnmount = this.componentWillUnmount.bind(this);
        this.loadVendorLibAsync = this.loadVendorLibAsync.bind(this);
        this.getRequestToken = this.getRequestToken.bind(this);
        this.showLightBox = this.showLightBox.bind(this);
    }
    loadVendorLibAsync() {
        const testInterval = 500;
        const timeToWait = 10000;
        let waitingFor = 0;
        const timeWaiting = () => {
            waitingFor += testInterval;
            return waitingFor;
        };
        const mpIsLoaded = () => !!window.MasterPass;
        if (mpIsLoaded()) {
            this.setState({ hasVendorLib: true });
        }
        else {
            const script = document.createElement('script');
            script.src = 'https://sandbox.masterpass.com/dyn/js/switch/integration/MasterPass.client.js';
            script.async = true;
            document.body.appendChild(script);
            const awaitScriptLoadOrTimeout = setInterval(() => (mpIsLoaded() || timeWaiting() >= timeToWait) && (() => {
                clearInterval(awaitScriptLoadOrTimeout);
                this._isMounted && this.setState({ hasVendorLib: mpIsLoaded() });
            })(), testInterval);
        }
    }
    getRequestToken(attempt) {
        const retryMaybe = () => (attempt < 3 && this.getRequestToken(++attempt)) || this.setState({ wasSuccess: false, wasCancelled: false, wasFailure: true });
        const { merchantId, clientId } = this.props.config.options.auth;
        const xhr = http_1.getGetRequest(`https://api-qa.sagepayments.com/paymentsjs/v2/api/payment/masterpass?gatewayId=${merchantId}&callbackUrl=${encodeURIComponent(this.method.data.callbackUrl)}&totalAmount=${this.payment.data.totalAmount}&v=v2`);
        xhr.setRequestHeader("clientVersion", "0.3.11".replace(/\./g, ''));
        xhr.setRequestHeader("clientId", clientId);
        xhr.onreadystatechange = () => {
            if (this._isMounted && xhr.readyState === xhr.DONE) {
                try {
                    const { requestToken, checkoutId } = JSON.parse(xhr.responseText);
                    if (!(requestToken && checkoutId)) {
                        retryMaybe(); // <-- this
                    }
                    else {
                        this.method.data.requestToken = requestToken;
                        this.method.data.merchantCheckoutId = checkoutId;
                        this.setState({ hasRequestToken: true });
                    }
                }
                catch (ex) {
                    retryMaybe(); // <-- is
                }
            }
        };
        try {
            xhr.send();
        }
        catch (ex) {
            retryMaybe(); // <-- blergh
        }
    }
    componentDidMount() {
        this._isMounted = true;
        this.method = arrays_1.findMixinByName(this.props.config.methods)('masterpass');
        this.payment = arrays_1.findMixinByName(this.props.config.operations)('payment');
        this.loadVendorLibAsync();
        this.getRequestToken(1);
        this.props.submittableCallback([false, this.method.data], false, true); // <- disable billing/feature
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    showLightBox() {
        // masterpass library can call multiple callbacks (???) which makes react throw
        // warnings about calling .setState at unacceptably-funky moments.
        this.method.data.successCallback = (data) => {
            const { oauth_verifier, checkout_resource_url } = data;
            this.method.data.verifierToken = oauth_verifier;
            this.method.data.checkoutUrl = checkout_resource_url;
            !this.state.wasCancelled && !this.state.wasFailure && (() => {
                this.setState({ wasSuccess: true, wasCancelled: false, wasFailure: false });
                this.props.submittableCallback([true, this.method.data], false, true);
            })();
        };
        this.method.data.cancelCallback = () => {
            !this.state.wasSuccess && !this.state.wasFailure && (() => {
                this.setState({ wasSuccess: false, wasCancelled: true, wasFailure: false });
                this.props.submittableCallback([false, this.method.data]);
            })();
        };
        this.method.data.failureCallback = () => {
            !this.state.wasSuccess && !this.state.wasCancelled && (() => {
                this.setState({ wasSuccess: false, wasCancelled: false, wasFailure: true });
                this.props.submittableCallback([false, this.method.data]);
            })();
        };
        window.MasterPass.client.checkout(this.method.data);
    }
    render() {
        return (React.createElement("div", { className: "sps sps-form" },
            (!this.state.wasSuccess && this.state.hasRequestToken && this.state.hasVendorLib && this.showLightBox())
                || ((this.state.wasCancelled || this.state.wasFailure) && React.createElement("div", { className: "sps-form-caption" }, "Please Try Again"))
                || this.state.wasSuccess && React.createElement("div", { className: "sps-form-caption" }, "Authenticated!")
                || React.createElement("div", { className: "sps-form-caption" }, "Authenticating"),
            React.createElement("div", { className: "sps-form-inputs" },
                !(this.state.wasCancelled || this.state.wasFailure || this.state.wasSuccess) && React.createElement("img", { src: "https://qa.sagepayments.net/pay/ui/0.3.11/css/assets/img/loading.svg" }),
                (this.state.wasCancelled || this.state.wasFailure) && React.createElement("img", { src: "https://static.masterpass.com/dyn/img/btn/global/mp_chk_btn_147x034px.svg", style: { opacity: this.state.hasRequestToken && this.state.hasVendorLib && 1 || 0.3 }, onClick: this.state.hasRequestToken && this.state.hasVendorLib && this.showLightBox }),
                this.state.wasSuccess && React.createElement("div", null, "Some text saying we're ready to submit."))));
    }
}
exports.MasterpassForm = MasterpassForm;
