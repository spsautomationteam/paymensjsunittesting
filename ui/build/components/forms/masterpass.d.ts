/// <reference types="react" />
import React = require('react');
import { methodFormProps } from "../../interfaces/methodForm";
import { Mixin } from '@payjs/core/models/mixin';
export declare class MasterpassForm extends React.Component<methodFormProps, any> {
    constructor(props: methodFormProps);
    _isMounted: boolean;
    method: Mixin<any>;
    payment: Mixin<any>;
    loadVendorLibAsync(): void;
    getRequestToken(attempt: number): void;
    componentDidMount(): void;
    componentWillUnmount(): void;
    showLightBox(): void;
    render(): JSX.Element;
}
