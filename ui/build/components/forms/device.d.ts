/// <reference types="react" />
import React = require('react');
import { methodFormProps, methodFormState } from "../../interfaces/methodForm";
import { Mixin } from '@payjs/core/models/mixin';
import { LogFn } from '@payjs/core/utility/logging';
import { deviceFormState } from "../../interfaces/deviceForm";
export declare class DeviceForm extends React.Component<methodFormProps, deviceFormState & methodFormState> {
    constructor(props: methodFormProps);
    method: Mixin<any>;
    log: LogFn;
    inputElement: HTMLInputElement;
    isValid(trackData: string): boolean;
    reset(): void;
    checkValidityState(): void;
    handleFocus(): void;
    handleBlur(e: React.FormEvent<HTMLInputElement>): void;
    handleChange(e: React.FormEvent<HTMLInputElement>): void;
    readInput(): void;
    render(): JSX.Element;
}
