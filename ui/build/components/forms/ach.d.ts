/// <reference types="react" />
import React = require('react');
import { methodFormProps, methodFormState } from "../../interfaces/methodForm";
export declare class AchForm extends React.Component<methodFormProps, methodFormState> {
    constructor(props: methodFormProps);
    componentWillMount(): void;
    updateFormState(field: string): (update: [string, boolean]) => void;
    checkValidityState(): void;
    render(): JSX.Element;
}
