/// <reference types="react" />
import React = require('react');
import { featureFormProps, featureFormState } from '../../interfaces/featureForm';
export declare class BillingForm extends React.Component<featureFormProps, featureFormState> {
    constructor(props: featureFormProps);
    componentDidMount(): void;
    updateFormState(field: string): (update: [string, boolean]) => void;
    checkValidityState(): void;
    render(): JSX.Element;
}
