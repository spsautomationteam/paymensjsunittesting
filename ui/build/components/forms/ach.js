"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ach_1 = require("@payjs/core/validation/ach");
var keystrokes_1 = require("@payjs/core/utility/keystrokes");
var bootstrap_1 = require("../shared/bootstrap");
var text_1 = require("../fields/text");
var dropdown_1 = require("../fields/dropdown");
var confirmation_1 = require("../fields/confirmation");
class AchForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: { routingNumber: '', accountNumber: '', type: '', ack: false },
            valid: { routingNumber: false, accountNumber: false, type: false, ack: false }
        };
        this.updateFormState = this.updateFormState.bind(this);
        this.checkValidityState = this.checkValidityState.bind(this);
    }
    componentWillMount() {
        if (this.props.config.uiOptions.mockMethodData === true) {
            this.setState({
                value: { routingNumber: '056008849', accountNumber: '12345678901234', type: 'Checking', ack: true },
                valid: { routingNumber: true, accountNumber: true, type: true, ack: true }
            }, this.checkValidityState);
        }
    }
    updateFormState(field) {
        return ((update) => {
            const ret = this.state;
            ret['value'][field] = update[0];
            ret['valid'][field] = update[1];
            this.setState(ret);
            this.checkValidityState();
        });
    }
    checkValidityState() {
        this.props.submittableCallback([
            Object.keys(this.state.valid).every(f => this.state.valid[f]),
            {
                routingNumber: this.state.value.routingNumber,
                accountNumber: this.state.value.accountNumber,
                type: this.state.value.type,
                secCode: "WEB"
            }
        ]);
    }
    render() {
        return (React.createElement("div", { className: "sps sps-form" },
            React.createElement("div", { style: { minHeight: '45px' } },
                React.createElement("div", { className: "sps-form-caption pull-left" }, "Check Info")),
            React.createElement("div", { className: "sps-form-inputs" },
                React.createElement(text_1.Text, { name: "routingNumber", label: "Routing", placeholder: "", formatter: s => s, validator: ach_1.isValidRouting, constrainer: keystrokes_1.isAllowedKeystroke, type: 'tel', isValid: this.state.valid.routingNumber, disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, callback: this.updateFormState('routingNumber') }),
                React.createElement(text_1.Text, { name: "accountNumber", label: "Account", placeholder: "", formatter: s => s, validator: ach_1.isValidAccount, constrainer: keystrokes_1.isAllowedKeystroke, type: 'tel', isValid: this.state.valid.accountNumber, disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, callback: this.updateFormState('accountNumber') }),
                React.createElement(dropdown_1.Dropdown, { name: "type", label: "Type", placeholder: "Select Account Type", disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, isValid: this.state.valid.type, callback: this.updateFormState('type'), options: [{ text: 'Checking', value: 'Checking' }, { text: 'Savings', value: 'Savings' }] }),
                React.createElement(confirmation_1.Confirmation, { label: React.createElement("span", null,
                        "I agree to the ",
                        React.createElement(bootstrap_1.OverlayTrigger, { placement: "top", overlay: React.createElement(bootstrap_1.Tooltip, { id: 'sps-achTooltip' }, "By authorizing this transaction, customer agrees that merchant may convert this transaction into an Electronic Funds Transfer (EFT) transaction or paper draft, and to debit this account for the amount of the transaction. Additionally, in the event this draft or EFT is returned unpaid, a service fee, as allowable by law, will be charged to this account via EFT or draft.") },
                            React.createElement("a", { onClick: () => false }, "EFT terms & conditions")),
                        "."), disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, isValid: this.state.valid.ack, callback: this.updateFormState('ack') }))));
    }
}
exports.AchForm = AchForm;
