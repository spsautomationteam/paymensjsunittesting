"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var card_1 = require("@payjs/core/validation/card");
var keystrokes_1 = require("@payjs/core/utility/keystrokes");
var regex_1 = require("@payjs/core/utility/regex");
var cardFormatting_1 = require("../../utility/cardFormatting");
var text_1 = require("../fields/text");
var cardExpirationDate_1 = require("../hybrid/cardExpirationDate");
var cardType_1 = require("../shared/cardType");
var kount_1 = require("../shared/kount");
var cvvHelp_1 = require("../shared/cvvHelp");
var operations_1 = require("../../utility/operations");
class CardForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: { cardNumber: '', expirationDate: '', cvv: '' },
            valid: { cardNumber: false, expirationDate: false, cvv: operations_1.isVaultOnlyRequest(this.props.config.operations) },
            ccErrorText: null
        };
        this.updateFormState = this.updateFormState.bind(this);
        this.checkValidityState = this.checkValidityState.bind(this);
        this.creditCardCallback = this.creditCardCallback.bind(this);
    }
    componentWillMount() {
        if (this.props.config.uiOptions.mockMethodData === true) {
            this.setState({
                value: { cardNumber: '5454545454545454', expirationDate: '0928', cvv: '123' },
                valid: { cardNumber: true, expirationDate: true, cvv: true }
            }, this.checkValidityState);
        }
    }
    updateFormState(field) {
        return ((update) => {
            const ret = this.state;
            ret['value'][field] = update[0];
            ret['valid'][field] = update[1];
            this.setState(ret);
            this.checkValidityState();
        });
    }
    checkValidityState() {
        this.props.submittableCallback([
            Object.keys(this.state.valid).every(f => this.state.valid[f]),
            {
                number: regex_1.stripNonNumeric(this.state.value.cardNumber),
                expiration: regex_1.stripNonNumeric(this.state.value.expirationDate),
                cvv: regex_1.stripNonNumeric(this.state.value.cvv)
            }
        ]);
    }
    creditCardCallback(update) {
        this.updateFormState('cardNumber')(update);
        // cc's callback has a little ad hoc logic to it:
        if (update[1] === false) {
            let ccErrorText = 'Invalid entry';
            if (!card_1.isAllowedCard(this.props.config.uiOptions.allowedCards)(regex_1.stripNonNumeric(update[0]))) {
                ccErrorText = !!this.props.config.uiOptions.allowedCards.error
                    ? this.props.config.uiOptions.allowedCards.error.replace(/#type#/g, card_1.getCardType(update[0][0]))
                    : `Sorry, we do not accept ${card_1.getCardType(update[0][0])} at this time`;
            }
            this.setState({ ccErrorText });
        }
    }
    render() {
        return (React.createElement("div", { className: "sps sps-form" },
            React.createElement("div", { style: { minHeight: '45px' } },
                React.createElement("div", { className: "sps-form-caption pull-left" }, "Card Info"),
                !!this.props.config.swipeEnabled ? React.createElement("span", { className: 'pull-right' }, this.props.config.swipeEnabled) : null),
            React.createElement("div", { className: "sps-form-inputs" },
                React.createElement(text_1.Text, { name: "cardNumber", label: 'Number', placeholder: "", formatter: cardFormatting_1.formatCardNumber(this.props.config.uiOptions.delimiter === false ? '' : this.props.config.uiOptions.delimiter || '-'), validator: s => card_1.isValidCardNumber(this.props.config.uiOptions.allowedCards)(regex_1.stripNonNumeric(s)), constrainer: keystrokes_1.isAllowedKeystroke, errorText: this.state.ccErrorText, type: 'tel', maxLength: 16, isValid: this.state.valid.cardNumber, disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, callback: this.creditCardCallback, style: { borderRight: 'none' }, addOn: React.createElement(cardType_1.CardType, { type: this.state.value.cardNumber.slice(0, 1), allowedCards: Object.assign({}, { amex: true, disc: true, visa: true, mc: true }, this.props.config.uiOptions.allowedCards) }), addOnStyle: { paddingTop: '5px', paddingBottom: '1px' } }),
                React.createElement(cardExpirationDate_1.CardExpirationDate, { isValid: this.state.valid.expirationDate, disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, callback: this.updateFormState('expirationDate') }),
                operations_1.isVaultOnlyRequest(this.props.config.operations)
                    ? null
                    : (React.createElement(text_1.Text, { name: "cvv", label: React.createElement(cvvHelp_1.CvvHelp, { type: this.state.value.cardNumber.slice(0, 1) }), placeholder: "", formatter: s => s, validator: card_1.isValidCvv, constrainer: keystrokes_1.isAllowedKeystroke, type: 'tel', isValid: this.state.valid.cvv, disabled: this.props.config.isProcessing || this.props.config.uiOptions.mockMethodData === true, callback: this.updateFormState('cvv') })),
                this.props.config.uiOptions.doKount
                    && this.props.config.options.auth.merchantId
                    && this.props.config.options.auth.salt
                    ? React.createElement(kount_1.KountFrame, { salt: this.props.config.options.auth.salt.replace(/\W+/g, "").slice(0, 20), merchantId: this.props.config.options.auth.merchantId })
                    : null)));
    }
}
exports.CardForm = CardForm;
