"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var arrays_1 = require("@payjs/core/utility/arrays");
var logging_1 = require("@payjs/core/utility/logging");
var cardType_1 = require("../shared/cardType");
var cardFormatting_1 = require("../../utility/cardFormatting");
class DeviceForm extends React.Component {
    constructor(props) {
        super(props);
        this.method = arrays_1.findMixinByName(this.props.config.methods)('device');
        this.method.data.type = this.method.data.type || 'IDTech';
        this.log = logging_1.getLogger('ui/components/forms/device')(this.props.config);
        this.log(`Device type: ${this.method.data.type}`);
        this.state = {
            attempts: 0,
            swipeState: 'none',
            value: { type: this.method.data.type, data: '' },
            valid: { type: !!this.method.data.type, data: false }
        };
        this.reset = this.reset.bind(this);
        this.checkValidityState = this.checkValidityState.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
        this.readInput = this.readInput.bind(this);
    }
    isValid(trackData) {
        return (
        // this logic should probably be in @payjs/core/validation
        (this.state.value.type.toLowerCase().startsWith('idtech')
            && (trackData.endsWith('03'))
            && (trackData.startsWith('02') || trackData.startsWith('9F39')))
            || (this.state.value.type.toLowerCase() === 'magtekcenturion'
                && trackData.startsWith('%B')));
    }
    reset() {
        this.setState(state => ({
            attempts: ++state.attempts,
            swipeState: 'none',
            fauxValue: { cardNumber: '', expirationDate: '', cvv: '' },
            value: { type: this.method.data.type, data: '' },
            valid: { type: !!this.method.data.type, data: false }
        }));
    }
    checkValidityState() {
        // lol gross:
        this.setState(state => Object.assign({}, state, { swipeState: 'parsing', value: { type: state.value.type, data: this.inputElement.value } }), () => {
            const validData = this.isValid(this.state.value.data);
            this.setState(state => Object.assign({}, state, { swipeState: validData && 'successful' || 'failed', valid: { data: validData } }), () => {
                this.props.submittableCallback([
                    Object.keys(this.state.valid).every(f => this.state.valid[f]),
                    {
                        type: this.state.value.type,
                        data: this.state.value.data
                    }
                ], true); // <-- auto-submit (if valid)
            });
        });
    }
    handleFocus() {
        this.setState(state => Object.assign({}, state, { swipeState: 'ready' }));
    }
    handleBlur(e) {
        switch (this.state.swipeState) {
            case 'none':
            case 'ready':
                e.currentTarget.focus();
                break;
            case 'reading':
                this.setState(state => Object.assign({}, state, { swipeState: 'interrupted' }));
                break;
            default:
                break;
        }
    }
    handleChange(e) {
        // this only needs to fire off once:
        if (this.state.swipeState !== 'reading') {
            this.inputElement = e.target;
            this.setState(state => Object.assign({}, state, { swipeState: 'reading' }), () => {
                this.forceUpdate();
                this.readInput();
            });
        }
    }
    readInput() {
        let val = this.inputElement.value;
        const read = setInterval(() => {
            if (this.inputElement.value !== val) {
                // still coming in
                val = this.inputElement.value;
            }
            else {
                // done
                clearInterval(read);
                this.checkValidityState();
            }
        }, 500);
    }
    render() {
        return (React.createElement("div", { className: "sps sps-form" },
            React.createElement("div", { className: "sps-form-caption" }, (this.state.swipeState === 'none' && React.createElement("div", { key: this.state.swipeState }, "Loading..."))
                || (this.state.swipeState === 'ready' && React.createElement("div", { key: this.state.swipeState }, "Insert/swipe when ready"))
                || (this.state.swipeState === 'reading' && React.createElement("div", { key: this.state.swipeState }, "Reading card data..."))
                || (this.state.swipeState === 'interrupted' && React.createElement("div", { key: this.state.swipeState }, "Interrupted"))
                || (this.state.swipeState === 'parsing' && React.createElement("div", { key: this.state.swipeState }, "Processing..."))
                || (this.state.swipeState === 'failed' && React.createElement("div", { key: this.state.swipeState }, "Couldn't read from card"))
                || (this.state.swipeState === 'successful' && React.createElement("div", { key: this.state.swipeState }, "Card Info"))),
            (this.state.swipeState === 'none' || this.state.swipeState === 'ready' || this.state.swipeState === 'reading' || this.state.swipeState === 'parsing')
                && React.createElement("img", { src: "https://qa.sagepayments.net/pay/ui/0.3.11/css/assets/img/loading.svg" }),
            (this.state.swipeState === 'interrupted' || this.state.swipeState === 'failed')
                && React.createElement("button", { className: "sps btn btn-primary", type: "button", onClick: this.reset }, "Retry"),
            React.createElement("div", { className: "sps-form-inputs" }, this.state.swipeState === 'successful'
                ? React.createElement(FauxCard, { trackData: this.state.value.data.substring(this.state.value.data.indexOf(';') + 1, this.state.value.data.indexOf('=')), allowedCards: this.props.config.uiOptions.allowedCards })
                : React.createElement("input", { autoFocus: true, key: this.state.attempts, type: 'text', className: 'sps sps-swipe-input', onFocus: this.handleFocus, onBlur: this.handleBlur, onChange: this.state.swipeState === 'ready' ? this.handleChange : null, style: {
                        border: 'none',
                        height: '1px',
                        width: '1px',
                        display: 'inline-block',
                        color: 'transparent',
                        backgroundColor: 'transparent'
                    } }))));
    }
}
exports.DeviceForm = DeviceForm;
class FauxCard extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement("span", null,
            React.createElement("div", { className: `sps form-group has-success` },
                React.createElement("label", { className: "sps" }, "Number"),
                React.createElement("div", { className: 'sps input-group' },
                    React.createElement("input", { type: 'text', className: `sps form-control input-block sps-text-field disabled`, value: cardFormatting_1.maskCardNumber('X')(this.props.trackData.replace(/\*/g, '0')), disabled: true }),
                    React.createElement("span", { className: "sps input-group-addon", style: { paddingTop: '3px', paddingBottom: '3px' } },
                        React.createElement(cardType_1.CardType, { type: this.props.trackData.slice(0, 1), allowedCards: Object.assign({}, { amex: true, disc: true, visa: true, mc: true }, this.props.allowedCards) })))),
            React.createElement("div", { className: `sps form-group has-success` },
                React.createElement("label", { className: "sps" },
                    "Expiration Month",
                    React.createElement("select", { className: `sps form-control input-block sps-select disabled`, value: "default" },
                        React.createElement("option", { value: "default", disabled: true }, "XX")))),
            React.createElement("div", { className: `sps form-group has-success` },
                React.createElement("label", { className: "sps" },
                    "Expiration Year",
                    React.createElement("select", { className: `sps form-control input-block sps-select disabled`, value: "default" },
                        React.createElement("option", { value: "default" }, "XXXX"))))));
    }
}
