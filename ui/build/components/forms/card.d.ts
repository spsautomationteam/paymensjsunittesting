/// <reference types="react" />
import React = require('react');
import { methodFormProps, methodFormState } from "../../interfaces/methodForm";
export declare class CardForm extends React.Component<methodFormProps, methodFormState & {
    ccErrorText: string;
}> {
    constructor(props: methodFormProps);
    componentWillMount(): void;
    updateFormState(field: string): (update: [string, boolean]) => void;
    checkValidityState(): void;
    creditCardCallback(update: [string, boolean]): void;
    render(): JSX.Element;
}
