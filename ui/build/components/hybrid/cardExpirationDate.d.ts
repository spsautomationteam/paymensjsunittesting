/// <reference types="react" />
import React = require('react');
import { fieldProps, fieldState } from "../../interfaces/field";
import { formState } from "../../interfaces/form";
export declare class CardExpirationDate extends React.Component<fieldProps, fieldState & formState> {
    constructor(props: fieldProps);
    updateFormState(field: string): any;
    checkValidityState(): void;
    render(): JSX.Element;
}
