"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var date_1 = require("@payjs/core/utility/date");
var card_1 = require("@payjs/core/validation/card");
var error_1 = require("../shared/error");
var dropdown_1 = require("../fields/dropdown");
const months = [
    { text: '01 - Jan', value: '01' },
    { text: '02 - Feb', value: '02' },
    { text: '03 - Mar', value: '03' },
    { text: '04 - Apr', value: '04' },
    { text: '05 - May', value: '05' },
    { text: '06 - Jun', value: '06' },
    { text: '07 - Jul', value: '07' },
    { text: '08 - Aug', value: '08' },
    { text: '09 - Sep', value: '09' },
    { text: '10 - Oct', value: '10' },
    { text: '11 - Nov', value: '11' },
    { text: '12 - Dec', value: '12' }
];
const years = (() => {
    const ret = [];
    const currentYear = parseInt(date_1.getCurrentYear('YYYY'));
    for (let i = currentYear; i < (currentYear + 15); i++) {
        ret.push({ text: i.toString(), value: i.toString().slice(-2) });
    }
    return ret;
})(); // iife
class CardExpirationDate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pristine: true,
            value: { expirationMonth: '', expirationYear: '' },
            valid: { expirationMonth: false, expirationYear: false }
        };
        this.updateFormState = this.updateFormState.bind(this);
        this.checkValidityState = this.checkValidityState.bind(this);
    }
    updateFormState(field) {
        return ((update) => {
            const ret = this.state;
            ret['value'][field] = update[0];
            ret['valid'][field] = update[1];
            this.setState(ret);
            this.checkValidityState();
        }).bind(this);
    }
    checkValidityState() {
        const exp = [this.state.value.expirationMonth, this.state.value.expirationYear];
        if (exp[0] && exp[1]) {
            const allValid = card_1.isValidExpirationDate(exp);
            this.setState({ pristine: false });
            this.props.callback([exp.join(''), allValid]);
        }
    }
    render() {
        return (React.createElement("div", { className: `sps form-group ${this.state.pristine ? '' : this.props.isValid ? 'has-success' : 'has-error'}`, style: { height: '57px' } },
            React.createElement("span", { style: { display: 'inline-block', width: '100%', height: '57px' } },
                React.createElement("div", { className: 'sps pull-left', style: { width: '48%' } },
                    React.createElement(dropdown_1.Dropdown, { name: "expirationMonth", label: "Expiration Month", placeholder: "Select Month", isValid: this.state.valid.expirationMonth, disabled: this.props.disabled, callback: this.updateFormState('expirationMonth'), options: months })),
                React.createElement("div", { className: 'sps pull-right', style: { width: '48%' } },
                    React.createElement(dropdown_1.Dropdown, { name: "expirationYear", label: "Expiration Year", placeholder: "Select Year", isValid: this.state.valid.expirationYear, disabled: this.props.disabled, callback: this.updateFormState('expirationYear'), options: years }))),
            this.state.pristine || this.props.isValid || React.createElement(error_1.Error, { message: "Credit card expired" })));
    }
}
exports.CardExpirationDate = CardExpirationDate;
