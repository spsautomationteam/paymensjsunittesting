"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
class Confirmation extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        const isChecked = e.target.checked;
        this.props.callback([isChecked.toString(), isChecked]);
    }
    render() {
        return (React.createElement("div", { className: 'sps form-group' },
            React.createElement("label", null,
                React.createElement("input", { className: `sps sps-checkbox ${this.props.disabled ? 'disabled' : ''}`, style: { opacity: 1 }, type: "checkbox", onChange: this.handleChange }),
                this.props.label)));
    }
}
exports.Confirmation = Confirmation;
