/// <reference types="react" />
import React = require('react');
import { dropdownFieldProps, dropdownFieldState } from "../../interfaces/dropdownField";
export declare class Dropdown extends React.PureComponent<dropdownFieldProps, dropdownFieldState> {
    constructor(props: dropdownFieldProps);
    handleChange(e: React.FormEvent<HTMLSelectElement>): void;
    render(): JSX.Element;
}
