"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var error_1 = require("../shared/error");
class Text extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            pristine: !this.props.value,
            preformattedValue: this.props.value || '',
            value: this.props.value || ''
        };
        this.handleInput = this.handleInput.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleFocus = this.handleFocus.bind(this);
        this.handleBlur = this.handleBlur.bind(this);
    }
    handleInput(e) {
        this.props.constrainer(e) || e.preventDefault();
    }
    handleChange(e) {
        const value = e.target.value;
        this.setState({ value, preformattedValue: value });
        this.props.callback([value, this.props.validator(value)]);
    }
    handleBlur(e) {
        const val = e.target.value;
        this.setState({ value: this.props.formatter(val), pristine: val.length == 0 });
    }
    handleFocus() {
        this.setState({ value: this.state.preformattedValue });
    }
    render() {
        const mainInput = React.createElement("input", { type: this.props.type || 'text', className: `sps form-control input-block sps-text-field ${this.props.disabled ? 'disabled' : ''}`, id: 'sps-' + this.props.name, placeholder: this.props.placeholder, value: this.state.value, onKeyPress: this.handleInput, onChange: this.handleChange, onFocus: this.handleFocus, onBlur: this.handleBlur, disabled: this.props.disabled || false, maxLength: this.props.maxLength || null, style: Object.assign({}, this.props.style) });
        return (React.createElement("div", { className: `sps form-group ${this.state.pristine ? '' : this.props.isValid ? 'has-success' : 'has-error'}` },
            React.createElement("label", { className: "sps", style: { minHeight: '19px' } }, this.props.label),
            !!this.props.addOn
                ? React.createElement("div", { className: 'sps input-group' },
                    mainInput,
                    React.createElement("span", { className: "sps input-group-addon", style: Object.assign({ paddingTop: '3px', paddingBottom: '3px' }, this.props.addOnStyle) }, this.props.addOn))
                : mainInput,
            this.state.pristine || this.props.isValid || React.createElement(error_1.Error, { message: this.props.errorText || "Invalid entry" })));
    }
}
exports.Text = Text;
