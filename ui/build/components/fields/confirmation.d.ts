/// <reference types="react" />
import React = require('react');
import { fieldProps } from "../../interfaces/field";
export declare class Confirmation extends React.PureComponent<fieldProps, any> {
    constructor(props: fieldProps);
    handleChange(e: React.FormEvent<HTMLInputElement>): void;
    render(): JSX.Element;
}
