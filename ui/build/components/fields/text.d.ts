/// <reference types="react" />
import React = require('react');
import { textFieldProps, textFieldState } from "../../interfaces/textField";
export declare class Text extends React.PureComponent<textFieldProps, textFieldState> {
    constructor(props: textFieldProps);
    handleInput(e: React.FormEvent<HTMLInputElement>): void;
    handleChange(e: React.FormEvent<HTMLInputElement>): void;
    handleBlur(e: React.FormEvent<HTMLInputElement>): void;
    handleFocus(): void;
    render(): JSX.Element;
}
