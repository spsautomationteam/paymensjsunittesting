"use strict";
// adaptation of https://github.com/davidwaterston/react-simple-dropdown
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
class Dropdown extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { selected: this.props.selected || 'default', pristine: !this.props.selected };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        const val = e.target.value;
        if (val != 'default') {
            this.setState({ selected: val, pristine: false });
            this.props.callback([val, true]);
        }
    }
    render() {
        return (React.createElement("div", { className: `sps form-group ${this.state.pristine ? '' : this.props.isValid ? 'has-success' : 'has-error'}` },
            React.createElement("label", { className: "sps" }, this.props.label),
            React.createElement("select", { id: 'sps-' + this.props.name, className: `sps form-control input-block sps-select ${this.props.disabled ? 'disabled' : ''}`, value: this.state.selected, onChange: this.handleChange },
                React.createElement("option", { value: "default", key: "default", disabled: true }, this.props.placeholder),
                ",",
                this.props.options.map(option => React.createElement("option", { className: "sps sps-option", key: option.value, value: option.value }, option.text)))));
    }
}
exports.Dropdown = Dropdown;
