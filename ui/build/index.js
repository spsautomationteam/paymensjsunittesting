"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var ReactDOM = require("react-dom");
var arrays_1 = require("@payjs/core/utility/arrays");
var request_1 = require("@payjs/core/api/request");
var response_1 = require("@payjs/core/api/response");
var logging_1 = require("@payjs/core/utility/logging");
var methodFormMaps_1 = require("./utility/methodFormMaps");
var featureFormMaps_1 = require("./utility/featureFormMaps");
var operations_1 = require("./utility/operations");
var paymentMethodSelect_1 = require("./components/shared/paymentMethodSelect");
var paymentMethod_1 = require("./components/shared/paymentMethod");
var processingResult_1 = require("./components/shared/processingResult");
var bootstrap_1 = require("./components/shared/bootstrap");
var loadingBar_1 = require("./components/shared/loadingBar");
// this is the entry-point into the ui library! consuming dev passes in an object that sets the initial ui state (via the config prop)
exports.Render = (config) => {
    const log = logging_1.getLogger('ui/index')(config);
    log('UI.Render invoked');
    try {
        ReactDOM.render(!!config.uiOptions.modal ? React.createElement(ModalUI, { config: config }) : React.createElement(UI, { config: config }), document.getElementById(config.uiOptions.target) /*,
        callbackfn*/);
    }
    catch (ex) {
        log('Exception during rendering!');
        log(ex);
    }
};
class UI extends React.PureComponent {
    constructor(props) {
        super(props);
        this.footerElementStyle = {};
        this.log = logging_1.getLogger('ui/main')(props.config);
        this.log('Initializing UI component');
        this.setActiveMethod = this.setActiveMethod.bind(this);
        this.updateFeatureSubmittableState = this.updateFeatureSubmittableState.bind(this);
        this.updateMethodSubmittableState = this.updateMethodSubmittableState.bind(this);
        this.submit = this.submit.bind(this);
        this.retry = this.retry.bind(this);
        this.state = {
            operations: arrays_1.asArray(props.config.operations),
            features: arrays_1.asArray(props.config.features),
            methods: arrays_1.asArray(props.config.methods),
            activeMethod: arrays_1.asArray(props.config.methods)[0],
            options: props.config.options,
            uiOptions: props.config.uiOptions,
            callback: props.config.callback,
            isSubmittable: false,
            isMethodSubmittable: false,
            ignoreFeature: false,
            isFeatureSubmittable: false || !props.config.uiOptions.show || (!!props.config.uiOptions.show && props.config.uiOptions.show.length === 0),
            isProcessing: false,
            processingResult: null,
            swipeEnabled: !arrays_1.findMixinByName(props.config.methods)('device') || operations_1.isVaultOnlyRequest(props.operations)
                ? false
                : React.createElement(paymentMethod_1.PaymentMethod, { key: 'device', text: 'Swipe', isActive: false, updater: this.setActiveMethod, method: arrays_1.findMixinByName(props.config.methods)('device') })
        };
        this.log('Initial config: ' + JSON.stringify({
            operations: arrays_1.getMixinNamesOnly(props.config.operations),
            features: arrays_1.getMixinNamesOnly(props.config.features),
            methods: arrays_1.getMixinNamesOnly(props.config.methods),
            options: props.config.options,
            uiOptions: props.config.uiOptions
        }));
    }
    setActiveMethod(m) {
        if (!this.state.isProcessing && !this.state.processingResult) {
            this.log(`New active method: ${m.name}`);
            this.setState({ activeMethod: m, isSubmittable: false, ignoreFeature: false });
        }
    }
    updateFeatureSubmittableState(name) {
        return ((update) => {
            arrays_1.findMixinByName(this.props.config.features)(name).data = update[1]; // bad!
            const isFeatureSubmittable = update[0];
            const isMethodSubmittable = this.state.isMethodSubmittable;
            const isSubmittable = isFeatureSubmittable && isMethodSubmittable;
            this.log(`Submittability state: ${JSON.stringify({ isFeatureSubmittable, isMethodSubmittable, isSubmittable })}`);
            this.setState({ isFeatureSubmittable, isSubmittable });
        }).bind(this);
    }
    updateMethodSubmittableState(update, autoSubmit = false, ignoreFeature = false) {
        this.state.activeMethod.data = update[1]; // bad!
        const isMethodSubmittable = update[0];
        const isFeatureSubmittable = this.state.isFeatureSubmittable;
        const isSubmittable = (isFeatureSubmittable || ignoreFeature) && isMethodSubmittable;
        this.log(`Submittability state: ${JSON.stringify({ ignoreFeature, isFeatureSubmittable, isMethodSubmittable, isSubmittable })}`);
        this.setState({ isMethodSubmittable, isSubmittable, ignoreFeature });
        if (isSubmittable && autoSubmit) {
            this.submit();
        }
    }
    submit() {
        this.setState({ isProcessing: true, processingResult: null });
        request_1.Send({
            methods: this.state.activeMethod,
            operations: this.state.operations,
            features: this.state.features,
            options: this.state.options,
            callback: (err, xhr, data, hash) => {
                this.setState({
                    isProcessing: false,
                    processingResult: response_1.parseCallback(err, xhr, data, hash).processingResult
                });
                this.state.callback(err, xhr, data, hash);
                this.props.config.uiOptions.suppressResultPage === true && this.log('Result page was suppressed!');
            }
        });
    }
    retry() {
        this.setState({
            processingResult: null,
            isProcessing: false,
            isSubmittable: false,
            activeMethod: this.state.methods[0]
        });
    }
    render() {
        return (React.createElement("span", { className: "sps-holder" },
            // styles:
            this.props.config.uiOptions.disableStyles ||
                React.createElement("span", null,
                    React.createElement("link", { href: "https://fonts.googleapis.com/css?family=Roboto", rel: "stylesheet" }),
                    React.createElement("link", { href: 'https://qa.sagepayments.net/pay/ui/0.3.11/css/pay.min.css', rel: 'stylesheet' })),
            // pre-load these images so there's no delay later:
            React.createElement("span", { hidden: true },
                React.createElement("img", { src: "https://qa.sagepayments.net/pay/ui/0.3.11/css/assets/img/loading.svg" }),
                React.createElement("img", { src: "https://qa.sagepayments.net/pay/ui/0.3.11/css/assets/img/SignIn_Progress-Looping.gif" })),
            // this is a hack to stop gulp-purifycss from removing dynamically-added classes
            React.createElement("span", { className: "tooltip tooltip-inner tooltip-arrow sr-only modal-backdrop modal-open modal-dialog modal-content modal-header modal-title modal-footer modal-body modal-scrollbar-measure modal-lg modal-sm close fade in top right bottom left", hidden: true }),
            // approved/error/declined page:
            (!this.state.isProcessing && !!this.state.processingResult && this.props.config.uiOptions.suppressResultPage !== true) &&
                React.createElement(processingResult_1.ProcessingResult, { processingResult: this.state.processingResult, retryFn: () => { this.setState({ processingResult: null, isProcessing: false, activeMethod: this.state.methods[0] }); } }),
            // method toggle:
            (!this.state.processingResult || this.props.config.uiOptions.suppressResultPage === true) && this.state.methods.length > 1 &&
                (React.createElement(paymentMethodSelect_1.PaymentMethodSelect, { wasSubmitted: this.state.isProcessing || !!this.state.processingResult, formMethodState: {
                        methods: this.state.methods,
                        activeMethod: this.state.activeMethod,
                        isVaultRequest: operations_1.isVaultOnlyRequest(this.state.operations),
                        updater: this.setActiveMethod
                    } })),
            // forms:
            (!this.state.processingResult || this.props.config.uiOptions.suppressResultPage === true) &&
                (React.createElement("div", { className: "sps-forms" },
                    (this.state.uiOptions.show || []).map(f => featureFormMaps_1.getFeatureForm(this.state, f, this.updateFeatureSubmittableState(f.name))),
                    this.state.methods.map(m => methodFormMaps_1.getMethodForm(this.state, m, this.updateMethodSubmittableState)))),
            React.createElement("div", { className: "sps-footer" },
                React.createElement("div", null,
                    // loading bar:
                    (this.state.isProcessing || (!!this.state.processingResult && this.props.config.uiOptions.suppressResultPage === true)) &&
                        React.createElement(loadingBar_1.LoadingBar, null),
                    // submit button:
                    !this.state.isProcessing && !this.state.processingResult &&
                        React.createElement("button", { className: "btn btn-primary footer-element", type: "button", onClick: this.submit, disabled: !this.state.isSubmittable }, (operations_1.isPaymentRequest(this.state.operations) && !!this.state.operations[0].data && !!this.state.operations[0].data.totalAmount && `PAY: $${this.state.operations[0].data.totalAmount}`)
                            || 'Submit'),
                    // retry button:
                    !!this.state.processingResult && this.state.processingResult.toLowerCase() !== 'approved' && this.props.config.uiOptions.suppressResultPage !== true &&
                        React.createElement("button", { className: "btn btn-primary footer-element", type: "button", onClick: this.retry }, "Retry"),
                    // close button (when modal):
                    !!this.props.closeModal && !this.state.isProcessing && !!this.state.processingResult && this.state.processingResult.toLowerCase() === 'approved' && this.props.config.uiOptions.suppressResultPage !== true &&
                        React.createElement("button", { className: "sps btn btn-primary footer-element", type: "button", onClick: this.props.closeModal }, "Close")))));
    }
}
exports.UI = UI;
class ModalUI extends React.PureComponent {
    constructor(props) {
        super(props);
        this.log = logging_1.getLogger('ui/modal')(props.config);
        this.log('Initializing ModalUI component');
        this.state = { modalOpen: this.props.config.uiOptions.modal.autoShow || false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    openModal() {
        this.log('Opening modal window');
        this.setState({ modalOpen: true });
    }
    closeModal() {
        this.log('Closing modal window');
        this.setState({ modalOpen: false });
    }
    render() {
        const buttonStyle = {
            display: 'inline-block',
            marginBottom: '0',
            fontWeight: 'normal',
            textAlign: 'center',
            verticalAlign: 'middle',
            cursor: 'pointer',
            backgroundImage: 'none',
            border: '1px solid transparent',
            whiteSpace: 'nowrap',
            padding: '6px 12px',
            fontSize: '14px',
            lineHeight: '1.428',
            borderRadius: '0',
            minWidth: '130px',
            color: '#ffffff',
            backgroundColor: '#38c72a',
            borderColor: '#38c72a'
        };
        return (React.createElement("div", { className: 'sps-holder' },
            React.createElement("button", { onClick: this.openModal, style: Object.assign({}, buttonStyle, this.props.config.uiOptions.modal.buttonStyle), className: this.props.config.uiOptions.modal.buttonClass || '' }, this.props.config.uiOptions.modal.buttonText || 'Pay Now'),
            React.createElement(bootstrap_1.Modal, { show: this.state.modalOpen, onHide: this.closeModal, dialogClassName: 'sps-holder' },
                React.createElement(bootstrap_1.Modal.Header, { closeButton: true },
                    React.createElement(bootstrap_1.Modal.Title, null, this.props.config.uiOptions.modal.titleText || 'Payment Details')),
                React.createElement(bootstrap_1.Modal.Body, null,
                    React.createElement(UI, { config: this.props.config, closeModal: this.closeModal })))));
    }
}
exports.ModalUI = ModalUI;
