/// <reference types="react" />
import React = require('react');
import { Mixin } from '@payjs/core/models/mixin';
import { LogFn } from '@payjs/core/utility/logging';
import { uiProps, uiState } from './interfaces/ui';
export declare const Render: (config: uiState) => void;
export declare class UI extends React.PureComponent<uiProps, uiState> {
    constructor(props: uiProps);
    log: LogFn;
    footerElementStyle: {};
    setActiveMethod(m: Mixin<any>): void;
    updateFeatureSubmittableState(name: string): any;
    updateMethodSubmittableState(update: [boolean, object], autoSubmit?: boolean, ignoreFeature?: boolean): void;
    submit(): void;
    retry(): void;
    render(): JSX.Element;
}
export declare class ModalUI extends React.PureComponent<uiProps, any> {
    constructor(props: uiProps);
    log: LogFn;
    openModal(): void;
    closeModal(): void;
    render(): JSX.Element;
}
