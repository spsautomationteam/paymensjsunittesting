export interface errorProps {
    message: string;
}
export interface kountProps {
    salt: string;
    merchantId: string;
}
export interface cardTypeProps {
    type: string;
    allowedCards: {
        amex: boolean;
        disc: boolean;
        visa: boolean;
        mc: boolean;
    };
}
