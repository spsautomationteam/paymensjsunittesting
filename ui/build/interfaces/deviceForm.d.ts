export interface deviceFormState {
    attempts: number;
    swipeState: 'none' | 'ready' | 'reading' | 'interrupted' | 'parsing' | 'successful' | 'failed';
}
