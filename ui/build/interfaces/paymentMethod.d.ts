import { Mixin } from '@payjs/core/models/mixin';
export interface formMethodState {
    methods: Mixin<any>[];
    activeMethod: Mixin<any>;
    isVaultRequest: boolean;
    updater: (m: Mixin<any>) => void;
}
export interface paymentMethodSelectProps {
    formMethodState: formMethodState;
    wasSubmitted: boolean;
}
export interface paymentMethodProps {
    method: Mixin<any>;
    isActive: boolean;
    text: string;
    imgTag?: string;
    updater: (m: Mixin<any>) => void;
}
