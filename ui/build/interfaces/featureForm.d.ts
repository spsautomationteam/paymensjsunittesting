import { formProps, formState } from './form';
import { Mixin } from '@payjs/core/models/mixin';
export interface featureFormProps extends formProps {
    feature: Mixin<any>;
    hide: string[];
    disable: string[];
    isProcessing: boolean;
    ignoreFeature: boolean;
}
export interface featureFormState extends formState {
}
