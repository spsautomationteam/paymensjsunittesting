import { fieldProps, fieldState } from "./field";
export interface dropdownOptions {
    text: string;
    value: string;
}
export interface dropdownFieldProps extends fieldProps {
    options: dropdownOptions[];
    selected?: string;
}
export interface dropdownFieldState extends fieldState {
    selected: string;
}
