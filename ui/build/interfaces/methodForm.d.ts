import { formProps, formState } from "./form";
import { uiState } from "./ui";
export interface methodFormProps extends formProps {
    config: uiState;
}
export interface methodFormState extends formState {
}
