/// <reference types="react" />
import { Mixin } from '@payjs/core/models/mixin';
import { RequestOptions } from '@payjs/core/api/request';
import { ProcessingResult } from '@payjs/core/api/response';
export interface uiProps {
    [index: string]: any;
    config: uiState;
    closeModal?: () => void;
}
export interface uiState {
    [index: string]: any;
    methods: Mixin<any>[];
    activeMethod: Mixin<any>;
    operations: Mixin<any>[];
    features?: Mixin<any>[];
    options: RequestOptions;
    uiOptions: uiOptions;
    callback: (err: any, xhr: XMLHttpRequest, data: string, hash: string) => any;
    isSubmittable: boolean;
    isProcessing: boolean;
    processingResult: ProcessingResult;
    swipeEnabled: boolean | JSX.Element;
    ignoreFeature: boolean;
}
export interface uiOptions {
    target: string;
    show: Mixin<any>[];
    hide: string[][];
    disable: string[][];
    modal?: {
        autoShow?: boolean;
        titleText?: string;
        buttonStyle?: any;
        buttonClass?: string;
        buttonText?: string;
    };
    allowedCards?: {
        amex: boolean;
        disc: boolean;
        visa: boolean;
        mc: boolean;
        error?: string;
    };
    suppressResultPage?: boolean;
    disableStyles?: boolean;
    mockMethodData?: boolean;
    doKount?: boolean;
    delimiter?: string | boolean;
}
