export interface fieldProps {
    callback: (update: [string, boolean]) => void;
    isValid: boolean;
    disabled?: boolean;
    name?: string;
    label?: any;
    placeholder?: string;
    errorText?: string;
}
export interface fieldState {
    pristine: boolean;
}
