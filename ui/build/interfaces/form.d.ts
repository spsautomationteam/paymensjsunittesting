export interface formProps {
    submittableCallback: (update: [boolean, object], autoSubmit?: boolean, ignoreFeature?: boolean) => void;
}
export interface formState {
    value: any;
    valid: any;
}
