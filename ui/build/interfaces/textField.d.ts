/// <reference types="react" />
import React = require('react');
import { fieldProps, fieldState } from "./field";
export interface textFieldProps extends fieldProps {
    formatter: (s: string) => string;
    validator: (s: string) => boolean;
    constrainer: (e: React.SyntheticEvent<any> | KeyboardEvent) => boolean;
    value?: string;
    type?: string;
    maxLength?: number;
    addOn?: any;
    addOnStyle?: any;
    style?: any;
}
export interface textFieldState extends fieldState {
    pristine: boolean;
    preformattedValue: string;
    value: string;
}
