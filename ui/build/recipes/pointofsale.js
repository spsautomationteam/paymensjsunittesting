"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var device_1 = require("@payjs/core/methods/device");
var card_1 = require("@payjs/core/methods/card");
var payment_1 = require("@payjs/core/operations/payment");
exports.PointOfSale = {
    methods: [device_1.Device, card_1.CreditCard],
    operations: [payment_1.Payment],
    features: [],
    options: {},
    uiOptions: {
        show: [payment_1.Payment],
        hide: {
            'payment': ['preAuth', 'orderNumber', 'shippingAmount']
        },
        disable: {
            'payment': ['totalAmount', 'taxAmount', 'tipAmount']
        }
    }
};
