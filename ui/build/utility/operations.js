"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var arrays_1 = require("@payjs/core/utility/arrays");
exports.isPaymentRequest = (ops) => !!arrays_1.asArray(ops)[0] && arrays_1.asArray(ops)[0].name === 'payment';
exports.isVaultOnlyRequest = (ops) => !!arrays_1.asArray(ops)[0] && arrays_1.asArray(ops)[0].name === 'vault';
