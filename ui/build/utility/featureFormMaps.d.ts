/// <reference types="react" />
import React = require('react');
import { Mixin } from '@payjs/core/models/mixin';
import { uiState } from '../interfaces/ui';
export declare const getFeatureForm: (state: uiState, f: Mixin<any>, cb: (update: [boolean, object]) => void) => React.Component<{}, {}>;
