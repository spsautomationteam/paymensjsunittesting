"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var billing_1 = require("../components/forms/billing");
exports.getFeatureForm = (state, f, cb) => {
    const path = f.name;
    const map = {
        'billing': (React.createElement(billing_1.BillingForm, { key: f.name, feature: f, hide: !!state.uiOptions.hide && state.uiOptions.hide[f.name] || [], disable: !!state.uiOptions.disable && state.uiOptions.disable[f.name] || [], isProcessing: state.isProcessing, ignoreFeature: state.ignoreFeature, submittableCallback: cb }))
    };
    return (map[path]) || null;
};
