"use strict";
// this module maps methods onto ui components.
// making these associations in the methods themselves would
// introduce a ui dependency to the core lib, which is bad.
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var card_1 = require("../components/forms/card");
var ach_1 = require("../components/forms/ach");
var device_1 = require("../components/forms/device");
var masterpass_1 = require("../components/forms/masterpass");
var paymentMethod_1 = require("../components/shared/paymentMethod");
const getMapper = (map) => (key) => map[key];
exports.getMethodForm = (state, m, cb) => {
    const path = m.name;
    const map = getMapper({
        'card': React.createElement(card_1.CardForm, { key: path, config: state, submittableCallback: cb }),
        'ach': React.createElement(ach_1.AchForm, { key: path, config: state, submittableCallback: cb }),
        'device': React.createElement(device_1.DeviceForm, { key: path, config: state, submittableCallback: cb }),
        'masterpass': React.createElement(masterpass_1.MasterpassForm, { key: path, config: state, submittableCallback: cb })
    });
    return state.activeMethod == m && map(path);
};
exports.getPaymentMethodSelect = (state, m, isVault) => {
    const path = m.name;
    const isActive = state.activeMethod == m;
    const map = {
        'card': React.createElement(paymentMethod_1.PaymentMethod, { key: path, text: 'Credit Card', imgTag: 'CC', isActive: isActive, updater: state.updater, method: m }),
        'ach': React.createElement(paymentMethod_1.PaymentMethod, { key: path, text: 'Check', imgTag: 'ACH', isActive: isActive, updater: state.updater, method: m }),
        'masterpass': !isVault && React.createElement(paymentMethod_1.PaymentMethod, { key: path, text: 'Masterpass', imgTag: 'MP', isActive: isActive, updater: state.updater, method: m })
    };
    return map[path];
};
