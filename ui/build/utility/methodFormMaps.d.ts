/// <reference types="react" />
import React = require('react');
import { uiState } from '../interfaces/ui';
import { formMethodState } from '../interfaces/paymentMethod';
import { Mixin } from '@payjs/core/models/mixin';
import { methodFormProps, methodFormState } from '../interfaces/methodForm';
export declare const getMethodForm: (state: uiState, m: Mixin<any>, cb: (update: [boolean, object]) => void) => React.Component<methodFormProps, methodFormState>;
export declare const getPaymentMethodSelect: (state: formMethodState, m: Mixin<any>, isVault: boolean) => any;
