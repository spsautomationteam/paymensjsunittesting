"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var strings_1 = require("@payjs/core/utility/strings");
var regex_1 = require("@payjs/core/utility/regex");
var card_1 = require("@payjs/core/validation/card");
// number:
exports.formatCardNumber = (delimiter = '-') => {
    return (cc) => {
        const _cc = regex_1.stripNonNumeric(cc.toString());
        const positions = card_1.isAmex(cc) && [4, 10, 15] || [4, 8, 12, 16];
        return strings_1.getDelimitingFunction(delimiter)(positions)(_cc);
    };
};
exports.maskCardNumber = (maskChar = 'X') => {
    return (cc) => {
        const _cc = regex_1.stripNonNumeric(cc.toString());
        const positions = [7, 8, 9, 10, 11, ...(card_1.isAmex(_cc) && [] || [12])].map(i => i - 1); // 1-index for readability, then map -1
        return strings_1.getMaskingFunction(maskChar)(positions)(_cc);
    };
};
// expiration:
exports.formatExpirationDate = (delimiter = '/') => (exp) => {
    const _exp = regex_1.stripNonNumeric(exp.toString());
    return strings_1.getDelimitingFunction(delimiter)([2])(_exp);
};
