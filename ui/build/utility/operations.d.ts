import { Mixin } from '@payjs/core/models/mixin';
export declare const isPaymentRequest: (ops: Mixin<any> | Mixin<any>[]) => boolean;
export declare const isVaultOnlyRequest: (ops: Mixin<any> | Mixin<any>[]) => boolean;
