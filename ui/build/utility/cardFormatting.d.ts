export declare const formatCardNumber: (delimiter?: string) => (s: string) => string;
export declare const maskCardNumber: (maskChar?: string) => (s: string) => string;
export declare const formatExpirationDate: (delimiter?: string) => (exp: string | number) => string;
