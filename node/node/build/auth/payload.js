const asArray = require('@payjs/core/utility/arrays').asArray;

const getPayload = (config) => {
    const ret = { auth: (!!config.options && !!config.options.auth) || null };
    [...asArray(config.operations), ...asArray(config.features)].forEach(m => { 
        ret[m.name.toLowerCase()] = m.data 
    });
    return JSON.stringify(ret);
}

module.exports.getPayload = getPayload;