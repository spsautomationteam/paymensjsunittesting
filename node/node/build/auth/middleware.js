const { getRandomData, encrypt } = require('./encryption');
const isStrAndStr = require('../utility/strings').isStrAndReturnStr

module.exports = (auth, random, encryptor) => {
    const { merchantId, merchantKey, clientId, clientKey, requestId } = auth;
    return (req, res, next) => {
        // random and encryptor are parameters for injecting mocks.
        // consumers of this method should *NOT* provide them.
        random = random || getRandomData(16);
        encryptor = encryptor || encrypt;    
        
        // extend:
        const requestCopy = Object.assign({}, req.body, { auth: Object.assign({}, req.body.auth, { merchantId, merchantKey, clientId, clientKey, requestId }) });
        requestCopy.auth.salt = random.salt;
        requestCopy.auth.requestId = isStrAndStr(requestCopy.auth.requestId) || getRandomData(10).salt;
        
        // delete fields which are both (a) not returned to client, and (b) not part of authKey
        delete requestCopy.auth.clientKey;
        //delete requestCopy.auth.url;
        
        // encrypt:
        requestCopy.auth.authKey = encryptor(JSON.stringify(requestCopy), clientKey, random);
        
        // delete fields which are not returned to client but ARE part of authKey:
        delete requestCopy.auth.merchantKey;
        
        // add response and bump:
        res.payjs = { auth: requestCopy.auth };
        next();
    }
};
