const crypto = require('crypto-js');
const getPayload = require('./payload').getPayload;

const getRandomData = (i = 16) => { 
    const iv = crypto.lib.WordArray.random(i);
    return {
        iv,
        salt: crypto.enc.Base64.stringify(crypto.enc.Utf8.parse(crypto.enc.Hex.stringify(iv)))
    }
}

const encrypt = (_message, secret, random = getRandomData(16)) => {
    let message = _message;
    if (typeof message === 'object') {
        message = getPayload(_message);
    }
    const derivedPassword = crypto.PBKDF2(secret, random.salt, { keySize: 256/32, iterations: 1500, hasher: (crypto.algo).SHA1 });
    const encrypted = crypto.AES.encrypt(message, derivedPassword, { iv: random.iv });
    return encrypted.toString();
}

module.exports.getRandomData = getRandomData;
module.exports.encrypt = encrypt;