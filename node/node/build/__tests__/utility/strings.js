const s = require('../../utility/strings');

let someString;
let someNumber;
let someBoolean;
let someObject;
let someFunction;

beforeEach(() => { 
    someString = Date.now().toString()
    someNumber = Math.floor((Math.random() * 100));
    someBoolean = Math.random() >= 0.5;
    someObject = { someString, someNumber, someBoolean };
    someFunction = () => someObject;
})

test('isStrAndReturnStr returns a string that was passed in', () => {
    expect(s.isStrAndReturnStr(`ceci n'est pas une string`)).toBe(`ceci n'est pas une string`);
    expect(s.isStrAndReturnStr(someString)).toBe(someString);
})

test('isStrAndReturnStr returns false when a number is passed in', () => {
    expect(s.isStrAndReturnStr(42)).toBe(false);
    expect(s.isStrAndReturnStr(0.5)).toBe(false);
    expect(s.isStrAndReturnStr(someNumber)).toBe(false);
})

test('isStrAndReturnStr returns false when a boolean is passed in', () => {
    expect(s.isStrAndReturnStr(true)).toBe(false); // lol
    expect(s.isStrAndReturnStr(false)).toBe(false);
    expect(s.isStrAndReturnStr(someBoolean)).toBe(false);
})

test('isStrAndReturnStr returns false when an object is passed in', () => {
    expect(s.isStrAndReturnStr({})).toBe(false);
    expect(s.isStrAndReturnStr({ foo: 'bar' })).toBe(false);
    expect(s.isStrAndReturnStr(someObject)).toBe(false);
})

test('isStrAndReturnStr returns false when a function is passed in', () => {
    expect(s.isStrAndReturnStr(() => 'even if it returns a string')).toBe(false);
    expect(s.isStrAndReturnStr(someFunction)).toBe(false);
})

test('isStrAndReturnStr returns false when nothing is passed in', () => {
    expect(s.isStrAndReturnStr()).toBe(false);
})

test('isStrAndReturnStr returns false when an empty string is passed in', () => {
    // is this correct? an empty string is still a string. ( typeof '' => "string" )
    // imo the behavior is correct but the name is (somewhat pedantically) imprecise
    expect(s.isStrAndReturnStr('')).toBe(false);
})

test('isStrAndReturnStr returns false when null is passed in', () => {
    expect(s.isStrAndReturnStr(null)).toBe(false);
})

test('isStrAndReturnStr returns false when undefined is passed in', () => {
    expect(s.isStrAndReturnStr(void 0)).toBe(false);
})


// Added New Test Cases

test('spyon isStrAndReturnStr function', () => {
  const spy = jest.spyOn(s, 'isStrAndReturnStr');
  const fn = s.isStrAndReturnStr();
  expect(spy).toHaveBeenCalled();
  expect(fn).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('strings module exports a single object', () => {   
  expect(typeof s).toBe('object');
 
})

test('s.isStrAndReturnStr function returns a single function', () => { 
  expect(typeof s.isStrAndReturnStr).toBe('function');
  
})

test('s.isStrAndReturnStr() function returns a boolean', () => {  
  expect(typeof s.isStrAndReturnStr()).toBe('boolean');
  
})

test('functions module exports a single object', () => {  
 
	expect(JSON.stringify(Object.keys(s))).toBe('["isStrAndReturnStr"]');
	expect(Object.keys(s).length).toBe(1);
  
})