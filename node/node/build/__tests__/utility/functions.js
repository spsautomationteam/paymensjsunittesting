const f = require('../../utility/functions');

let someFunction;
let someString;
let someNumber;
let someBoolean;
let someObject;

beforeEach(() => { 
    someString = Date.now().toString()
    someNumber = Math.floor((Math.random() * 100));
    someBoolean = Math.random() >= 0.5;
    someObject = { someString, someNumber, someBoolean };
    someFunction = () => someObject;
	
//	console.log(someObject);
//	console.log(someFunction);
	
})

test('isFnAndCallFn calls a function that was passed in', () => {
    let ret;	
    ret = f.isFnAndCallFn(() => someString);	
    expect(ret).toBe(someString);
    ret = f.isFnAndCallFn(someFunction);
    expect(ret).toBe(someObject);
})

test('isFnAndCallFn does not call a function with any arguments', () => {
    let ret;
    ret = f.isFnAndCallFn(foo => foo);
    expect(ret).toBe(void 0);
    ret = f.isFnAndCallFn((foo = someString) => foo);
    expect(ret).toBe(someString);
})

test('isFnAndCallFn returns false when a string is passed in', () => {
    expect(f.isFnAndCallFn(`ceci n'est pas une string`)).toBe(false);
    expect(f.isFnAndCallFn(someString)).toBe(false);
})

test('isFnAndCallFn returns false when a number is passed in', () => {
    expect(f.isFnAndCallFn(42)).toBe(false);
    expect(f.isFnAndCallFn(0.5)).toBe(false);
    expect(f.isFnAndCallFn(someNumber)).toBe(false);
})

test('isFnAndCallFn returns false when a boolean is passed in', () => {
    expect(f.isFnAndCallFn(true)).toBe(false); // lol
    expect(f.isFnAndCallFn(false)).toBe(false);
    expect(f.isFnAndCallFn(someBoolean)).toBe(false);
})

test('isFnAndCallFn returns false when an object is passed in', () => {
    expect(f.isFnAndCallFn({})).toBe(false);
    expect(f.isFnAndCallFn({ foo: 'bar' })).toBe(false);
    expect(f.isFnAndCallFn(someObject)).toBe(false);
})

test('isFnAndCallFn returns false when nothing is passed in', () => {
    expect(f.isFnAndCallFn()).toBe(false);
})

test('isFnAndCallFn returns false when an empty string is passed in', () => {
    // is this correct? an empty string is still a string. ( typeof '' => "string" )
    // imo the behavior is correct but the name is (somewhat pedantically) imprecise
    expect(f.isFnAndCallFn('')).toBe(false);
})

test('isFnAndCallFn returns false when null is passed in', () => {
    expect(f.isFnAndCallFn(null)).toBe(false);
})

test('isFnAndCallFn returns false when undefined is passed in', () => {
    expect(f.isFnAndCallFn(void 0)).toBe(false);
})

// Added New Test Cases

test('spyon isFnAndCallFn function', () => {
  const spy = jest.spyOn(f, 'isFnAndCallFn');
  const fn = f.isFnAndCallFn();
  expect(spy).toHaveBeenCalled();
  expect(fn).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('functions module exports a single object', () => {   
  expect(typeof f).toBe('object');
 
})

test('f.isFnAndCallFn function returns a single function', () => { 
  expect(typeof f.isFnAndCallFn).toBe('function');
  
})

test('f.isFnAndCallFn() function returns a boolean', () => {  
  expect(typeof f.isFnAndCallFn()).toBe('boolean');
  
})

test('functions module exports a single object', () => {  
 
	expect(JSON.stringify(Object.keys(f))).toBe('["isFnAndCallFn"]');
	expect(Object.keys(f).length).toBe(1);
	
 // console.log(test,'-----------------------',f);  
 // console.log('-----------------------',f.isFnAndCallFn);
 // console.log('-----------------------',f.isFnAndCallFn()); 	
  
})

