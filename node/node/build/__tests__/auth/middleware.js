const m = require('../../auth/middleware');

const mockAuth = {
    merchantId: 'foo',
    merchantKey: 'bar',
    clientId: 'baz',
    clientKey: 'qum',
    requestId: 'qux'
}

const mockRandom = {
    iv: 'foo',
    salt: 'bar'
}

const mockEncryptor = (obj, key, ran) => `${ran.iv}|${ran.salt}|${key}`

test('middleware module exports a single function', () => {
    expect(typeof m).toBe('function');
})

test('middleware module exports a single function that returns a mockAuth function', () => {
    const mw = m(mockAuth);
    expect(typeof mw).toBe('function');
	expect(typeof mw).not.toBeNull();
	expect(typeof mw).not.toBe('object');

})

test('middleware function doesn\'t mutate req', () => {
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: 'yo' };
    const reqCopy = Object.assign({}, req);
    const res = {};
    const next = () => {};
    mw(req, res, next);
    expect(req).toEqual(reqCopy);
})

test('middleware function calls next', (done) => {
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: 'yo' };
    const res = {};
    const next = () => {
        done();
    };
    mw(req, res, next);
})

test('middleware function updates res with .payjs and .payjs.auth', () => {
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: 'yo' };
    const res = {};
    const next = () => {};
    mw(req, res, next);
    expect(res.payjs).toBeDefined();
    expect(res.payjs.auth).toBeDefined();
})

test('middleware function creates res.payjs.auth with the appropriate data', () => {
    const url = Date.now().toString();
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: { auth: { url } } };
    const res = {};
    const next = () => {};
    mw(req, res, next);
	//console.log(res);

    expect(res.payjs.auth.merchantId).toBeDefined();
    expect(res.payjs.auth.merchantId).toBe(mockAuth.merchantId);

    expect(res.payjs.auth.requestId).toBeDefined();
    expect(res.payjs.auth.requestId).toBe(mockAuth.requestId)

    expect(res.payjs.auth.clientId).toBeDefined();
    expect(res.payjs.auth.clientId).toBe(mockAuth.clientId)

    expect(res.payjs.auth.salt).toBeDefined();
    expect(res.payjs.auth.salt).toBe(mockRandom.salt);
	
	expect(res.payjs.auth.iv).not.toBeDefined();

    expect(res.payjs.auth.authKey).toBeDefined();
    expect(res.payjs.auth.authKey).toBe(mockEncryptor({}, mockAuth.clientKey, mockRandom));

    expect(res.payjs.auth.url).toBeDefined();
    expect(res.payjs.auth.url).toBe(url);
})

test('middleware function creates res.payjs.auth without sensitive data', () => {
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: { auth: { url: 'foo'} } };
    const res = {};
    const next = () => {};
    mw(req, res, next);

    expect(res.payjs.auth.merchantKey).not.toBeDefined();
    expect(res.payjs.auth.clientKey).not.toBeDefined();
})

// added new test cases


test('middleware module exports a single function and it should not be null', () => {
  
  expect(m).not.toBeNull();
  
})

test('middleware module exports a single function and returned arguments count should be 3', () => {
    expect(m.length).not.toBeNull();
    expect(m.length).toBe(3);
})

test('middleware module exports a single function that returns a mockRandom function', () => {
    const mw = m(mockRandom);
    expect(typeof mw).toBe('function');
	expect(typeof mw).not.toBeNull();
	expect(typeof mw).toBeDefined();
	
})

test('middleware module exports a single function that returns a mockEncryptor function', () => {
    const mw = m(mockEncryptor);
    expect(typeof mw).toBe('function');
	expect(typeof mw).not.toBeNull();
	expect(typeof mw).toBeDefined();

})

test('middleware function creates res.payjs.auth with blank url data', () => {
    const url = "";
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: { auth: { url } } };
    const res = {};
    const next = () => {};
    mw(req, res, next);
//console.log(res);
    expect(res.payjs).toBeDefined();
    expect(res.payjs.auth).toBeDefined();
	
	expect(res.payjs.auth.url).toBeDefined();
	expect(res.payjs.auth.url).toEqual('');
	expect(res.payjs.auth.url).not.toBeNull();
	
})

test('middleware function creates res.payjs.auth with url data as null', () => {
    const url = null;
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: { auth: { url } } };
    const res = {};
    const next = () => {};
    mw(req, res, next);
	//console.log(res);
    expect(res.payjs).toBeDefined();
    expect(res.payjs.auth).toBeDefined();
	
	expect(res.payjs.auth.url).toBeDefined();
	expect(res.payjs.auth.url).toBeNull();
	
})

test('middleware function creates res.payjs.auth with properties and appropriate data types', () => {
    const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: { auth: { url: 'foo'} } };
    const res = {};
    const next = () => {};
    mw(req, res, next);
//console.log(res);

	expect(typeof res.payjs.auth.merchantId).toBe('string');
	expect(typeof res.payjs.auth.requestId).toBe('string');
	expect(typeof res.payjs.auth.requestId).toBe('string');
	expect(typeof res.payjs.auth.salt).toBe('string');
	expect(typeof res.payjs.auth.authKey).toBe('string');
	expect(typeof res.payjs.auth.url).toBe('string');
    
})

test('middleware function creates res.payjs.auth by passing mockAuth, mockRandom, mockEncryptor', () => {
    const url = Date.now().toString();
//    const mw = m(mockAuth, mockRandom, mockEncryptor);
	const mw = m(mockAuth, mockRandom, mockEncryptor);
    const req = { body: { auth: { url } } };
    const res = {};
    const next = () => {};
    mw(req, res, next);
//console.log(res);

	expect(res.payjs.auth.url).not.toEqual('');
	expect(res.payjs.auth.url).not.toBeNull();	

	expect(res.payjs.auth.merchantId).toBe('foo');
	expect(res.payjs.auth.clientId).toBe('baz');	
	expect(res.payjs.auth.requestId).toBe('qux'); 
	expect(res.payjs.auth.salt).toBe('bar');	
	expect(res.payjs.auth.authKey).toBe(mockEncryptor({}, mockAuth.clientKey, mockRandom));    
	
})

test('middleware function creates res.payjs.auth without passing mockRandom, mockEncryptor', () => {
    const url = Date.now().toString();
	const mw = m(mockAuth);
    const req = { body: { auth: { url } } };
    const res = {};
    const next = () => {};
    mw(req, res, next);
//console.log(res);

	expect(res.payjs.auth.url).not.toEqual('');
	expect(res.payjs.auth.url).not.toBeNull();	

	expect(res.payjs.auth.merchantId).toBe('foo');
	expect(res.payjs.auth.clientId).toBe('baz');	
	expect(res.payjs.auth.requestId).toBe('qux'); 

	expect(res.payjs.auth.salt).not.toBe('bar');	
	expect(res.payjs.auth.authKey).not.toBe(mockEncryptor({}, mockAuth.clientKey, mockRandom));       
	
})

test('middleware function creates res.payjs.auth with fixed size of authKey and url', () => { 	
	for (i = 1; i < 10; i++) {  
		const url = Date.now().toString();
		const mw = m(mockAuth);
		const req = { body: { auth: { url } } };
		const res = {};
		const next = () => {};
		mw(req, res, next);
//	console.log(res);
		expect(res.payjs.auth.authKey.length).not.toBeNull();  
		expect(res.payjs.auth.authKey.length).toBe(236); 
		expect(res.payjs.auth.authKey.length).not.toBeGreaterThan(236);	
		expect(res.payjs.auth.authKey.length).not.toBeLessThan(236);		
		
		expect(res.payjs.auth.url.length).not.toBeNull();  
		expect(res.payjs.auth.url.length).toBe(13);
		expect(res.payjs.auth.url.length).not.toBeGreaterThan(13);	
		expect(res.payjs.auth.url.length).not.toBeLessThan(13); 		
	}
})


