const _ = require('underscore');
const m = require('../../auth/payload');

const getRandomString = (i = 10) => _.sample('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', i).join('');
const getFakeModule = () => {
    const ret = { name: getRandomString(), data: {} };
    ret.data[getRandomString()] = getRandomString();
    ret.data[getRandomString()] = getRandomString();
    ret.data[getRandomString()] = getRandomString();
    return ret;
}
const getFakePayload = (config) => {
    const ret = {  };
    const ops = (Array.isArray(config.operations) && config.operations) || [config.operations];
    const fts = (Array.isArray(config.features) && config.features) || [config.features];
    ret['auth']  = (!!config.options && !!config.options.auth) || null;
    [...ops, ...fts].forEach(m => { ret[m.name.toLowerCase()] = m.data });
    return ret;
}

test('getPayload converts a configuration object into the JSON string that needs encryption', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);
    
    expect(JSON.parse(testString)).toEqual(getFakePayload(config));
    config.operations.forEach(m => { Object.keys(m.data).map(k => m.data[k]).forEach(d => { expect(testString.indexOf(d)).not.toBe(-1) }); })
    config.features.forEach(m => { Object.keys(m.data).map(k => m.data[k]).forEach(d => { expect(testString.indexOf(d)).not.toBe(-1) }); })
})

test('getPayload will convert single modules into arrays', () => {
    const m1 = getFakeModule();
    const o1 = getFakeModule();
    const f1 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: m1,
        operations: o1,
        features: f1,
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);
    // compare deserialzed versions:
    expect(JSON.parse(testString)).toEqual(getFakePayload(config));
})

test('getPayload sets auth to null when absent', () => {
    const m1 = getFakeModule();
    const o1 = getFakeModule();
    const f1 = getFakeModule();
    const config = {
        methods: m1,
        operations: o1,
        features: f1,
    }
    const testString = m.getPayload(config);
    // compare deserialzed versions:
//console.log("Checking null : "+JSON.parse(testString).auth);
    expect(JSON.parse(testString).auth).toBeNull();
})

test('getPayload does not include methods in its result', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const f1 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: o1,
        features: f1,
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);

    expect(JSON.parse(testString)[m1.name.toLowerCase()]).toBeUndefined();
    expect(JSON.parse(testString)[m2.name.toLowerCase()]).toBeUndefined();
    config.methods.forEach(m => { Object.keys(m.data).map(k => m.data[k]).forEach(d => { expect(testString.indexOf(d)).toBe(-1) }); })
    expect(JSON.parse(testString)[o1.name.toLowerCase()]).toBeDefined();
    expect(JSON.parse(testString)[f1.name.toLowerCase()]).toBeDefined();
    expect(JSON.parse(testString)['auth']).toBeDefined();
})

// Added New TestCases

test('SpyOn on getPayload function', () => {

	const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }    

  const spy = jest.spyOn(m, 'getPayload');
  const pl = m.getPayload(config);
  expect(spy).toHaveBeenCalled();
  expect(pl).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('getPayload module exports a single Object', () => {
	
	 expect(typeof m).toBe('object');

})


test('getPayload module exports a single object that returns a function', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }

	const pl = m.getPayload(config);
	
	expect(typeof pl).toBe('string');
    
})

test('getPayload sets auth value as true when it\'s present', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }
	
	const testString = m.getPayload(config);

    
	expect(JSON.parse(testString).auth).toBeDefined();
    expect(JSON.parse(testString).auth).toBe(true);
//		console.log(JSON.parse(testString))
//		console.log("len :"+Object.keys(JSON.parse(testString)).length); 
    
})

test('getPayload sets auth to be true and length should be 5 when \'config\' object contains all data', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);    
console.log("testString :  "+testString);
console.log("JSON.parse(testString) : "+JSON.parse(testString));
console.log("JSON.stringify(JSON.parse(testString)) : " +JSON.stringify(JSON.parse(testString)));
   
	expect(JSON.parse(testString).auth).toBe(true);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(5); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");

})

test('Verify getPayload keys length in Json result when methods data left blank', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }
	
	const testString = m.getPayload(config);
    
	expect(JSON.parse(testString).auth).toBe(true);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(5); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");
	
	//	console.log(JSON.parse(testString))
	//	console.log("len :"+Object.keys(JSON.parse(testString)).length); 
    
})

test('Verify getPayload keys length in Json result when methods and Operations data left blank', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [],
        operations: [],
        features: [f1, f2],
        options: {
            auth
        }
    }
	
	const testString = m.getPayload(config);
    
	expect(JSON.parse(testString).auth).toBe(true);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(3); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");
	
	//	console.log(JSON.parse(testString))
	//	console.log("len :"+Object.keys(JSON.parse(testString)).length); 
    
})

test('Verify getPayload keys length in Json result when all config fields data left blank', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [],
        operations: [],
        features: [],
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);  

expect(JSON.parse(testString).auth).toBe(true);
expect(JSON.stringify(JSON.parse(testString))).toBe('{"auth":true}');
expect(Object.keys(JSON.parse(testString)).length).toBe(1); 
//console.log("Auth : "+Object.keys(JSON.parse(testString)));
expect(JSON.stringify(Object.keys(JSON.parse(testString)))).toBe("[\"auth\"]");
    
})

test('Verify getPayload without providing methods data', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
      //  methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);    
//console.log(JSON.parse(testString));

	expect(JSON.parse(testString).auth).toBe(true);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(5); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");
    
})

 // TypeError: Cannot read property 'name' of undefined
test('Verify getPayload without providing operations data', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
     //  operations: [o1, o2],
        features: [f1, f2],
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);    
//console.log(JSON.parse(testString));

	expect(JSON.parse(testString).auth).toBe(true);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(5); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");
    
})
 
 // TypeError: Cannot read property 'name' of undefined
test('Verify getPayload without providing features data', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
    //    features: [f1, f2],
        options: {
            auth
        }
    }
    const testString = m.getPayload(config);    
//console.log(JSON.parse(testString));

	expect(JSON.parse(testString).auth).toBe(true);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(5); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");
    
})

test('Verify getPayload without providing Options data', () => {
    const m1 = getFakeModule();
    const m2 = getFakeModule();
    const o1 = getFakeModule();
    const o2 = getFakeModule();
    const f1 = getFakeModule();
    const f2 = getFakeModule();
    const auth = {
        foo: getRandomString()
    }
    const config = {
        methods: [m1, m2],
        operations: [o1, o2],
        features: [f1, f2],
       // options: {
         //   auth
        //}
    }
    const testString = m.getPayload(config);    
//console.log(JSON.parse(testString));

	expect(JSON.parse(testString).auth).toBe(null);
	expect(JSON.stringify(JSON.parse(testString))).not.toBe('{"auth":true}');
	expect(Object.keys(JSON.parse(testString)).length).toBe(5); 
	expect(Object.keys(JSON.parse(testString))).not.toBe("auth");
    
})

