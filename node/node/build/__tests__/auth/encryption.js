const e = require('../../auth/encryption');
const crypto = require('crypto-js');

test('this sample test fails intentionally', () => {
    expect(false).toBe(false);
})

test('getRandomData returns an object with "iv" and "salt" properties', () => {
    const r = e.getRandomData();
    expect(r.hasOwnProperty('iv')).toBe(true);
    expect(r.hasOwnProperty('salt')).toBe(true);
})

test('getRandomData returns an object with properties of the appropriate types', () => {
    const r = e.getRandomData();
    expect(Array.isArray(r.iv.words)).toBe(true);
    expect(typeof r.salt).toBe('string');
	expect(typeof r.iv.sigBytes).toBe('number');	

})

test('getRandomData returns an initialization vector of the specified length', () => {
    let r;
    r = e.getRandomData(16);
    expect(r.iv.words.length).toBe(16 / 4);
	//console.log(r);

    r = e.getRandomData(4);
    expect(r.iv.words.length).toBe(4 / 4);
	//console.log(r);

    r = e.getRandomData(8);
    expect(r.iv.words.length).toBe(8 / 4);
	//console.log(r);
})

test('getRandomData defaults to 16 if the length is not specified', () => {
    let r;
    r = e.getRandomData();
    expect(r.iv.words.length).toBe(16 / 4);
    r = e.getRandomData();
    expect(r.iv.words.length).toBe(16 / 4);
})

test('getRandomData returns a salt that is a base64 representation of its iv', () => {
    // this test is sensitive to CHANGE but not SOUNDNESS
    let r;
    r = e.getRandomData();
    expect(r.salt).toBe(crypto.enc.Base64.stringify(crypto.enc.Utf8.parse(crypto.enc.Hex.stringify(r.iv))));
    r = e.getRandomData();
    expect(r.salt).toBe(crypto.enc.Base64.stringify(crypto.enc.Utf8.parse(crypto.enc.Hex.stringify(r.iv))));
})

// Added New Test Cases

test('spyon getRandomData function', () => {
  const spy = jest.spyOn(e, 'getRandomData');
  const r = e.getRandomData();
  expect(spy).toHaveBeenCalled();
  expect(r).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('spyon encrypt function', () => {
  const spy = jest.spyOn(e, 'encrypt');
  const ee = e.encrypt();
  expect(spy).toHaveBeenCalled();
  expect(ee).not.toBeNull();
  spy.mockReset();
  spy.mockRestore();
  
})

test('encryption module exports a object', () => {
    expect(typeof e).toBe('object');
})

test('getRandomData returns an object only with "iv", "salt" Properties', () => {
    expect(Object.getOwnPropertyNames(e.getRandomData()).length).toBe(2);
    expect(JSON.stringify(Object.getOwnPropertyNames(e.getRandomData()))).toBe("[\"iv\",\"salt\"]");
	
})

//test('getRandomData does not returns other than the "iv" and "salt" properties', () => {
  //  const r = e.getRandomData();
//    expect(r.hasOwnProperty('test')).toBeUndefined();	
	//
//})

test('encryption module exports two functions these returns a function', () => {
	
	expect(typeof e.getRandomData).toBe('function');
	expect(typeof e.encrypt).toBe('function');	
   
})

test('getRandomData returns the salt with the fixed size of 44', () => {
	
	for (i = 1; i < 10; i++) {  
		const r = e.getRandomData();
//	console.log(r);
		expect(r.salt.length).not.toBeNull();  
		expect(r.salt.length).toBe(44);	
		expect(r.salt.length).not.toBeGreaterThan(44);	
		expect(r.salt.length).not.toBeLessThan(44);
	}
	
})

test('getRandomData returns the salt, iv and sigBytes properties and they should not be blank data', () => {
    let r;
	r = e.getRandomData();
    expect(r.iv.words).not.toBeNull();
	expect(r.salt).not.toBeNull();	
	expect(r.iv.sigBytes).not.toBeNull();	
	
	r = e.getRandomData(16);
    expect(r.iv.words).not.toBeNull();
	expect(r.salt).not.toBeNull();	
	expect(r.iv.sigBytes).not.toBeNull();	
})

test('getRandomData returns the iv and it\'s value shoult be within the boundry', () => {
    let r;
    r = e.getRandomData(16);
//console.log(r);
	expect(r.iv.words.length).not.toBeNull();
    expect(r.iv.words.length).toBe(16 / 4);
	expect(r.iv.words.length).not.toBeGreaterThan(16 / 4);
	expect(r.iv.words.length).not.toBeLessThan(16 / 4);
	
	expect(r.iv.sigBytes).toBe(16);	
   
})

test('check getRandomData function by passing values are less than 4, 8, 16', () => {
    let r;  

    r = e.getRandomData(3);
    expect(r.iv.words.length).toBe(1);
	//console.log(r);

    r = e.getRandomData(7);
    expect(r.iv.words.length).toBe(2);
	
	r = e.getRandomData(15);
    expect(r.iv.words.length).toBe(4);
	//console.log(r);
   
})

test('check getRandomData function by passing text data', () => {
    let r;
    r = e.getRandomData('abc');
//console.log(r);
	expect(r.iv.words.length).toBe(0);
	expect(r.iv.words.length).not.toBeGreaterThan(0);
	expect(r.iv.words).not.toBeNull();	
	
	expect(r.iv.sigBytes).toBe('abc');
	expect(typeof r.iv.sigBytes).toBe('string');
	
	expect(r.salt.length).toBe(0);
	expect(r.salt.length).not.toBeGreaterThan(0);
	expect(r.salt).not.toBeNull();
   
})

test('check getRandomData function by passing special characters data', () => {
    let r;
    r = e.getRandomData('@#$%^&*');
//console.log(r);
	expect(r.iv.words.length).toBe(0);
	expect(r.iv.words.length).not.toBeGreaterThan(0);
	expect(r.iv.words).not.toBeNull();	
	
	expect(r.iv.sigBytes).toBe('@#$%^&*');
	expect(typeof r.iv.sigBytes).toBe('string');

	expect(r.salt.length).toBe(0);
	expect(r.salt.length).not.toBeGreaterThan(0);
	expect(r.salt).not.toBeNull();
   
})

test('check getRandomData function by passing blank data', () => {
    let r;
    r = e.getRandomData(" ");
//console.log(r);	

	expect(r.iv.words.length).toBe(0);
	expect(r.iv.words).toEqual([]);
	
	expect(r.iv.sigBytes).toBe(" ");	

	expect(r.salt).toBe("");	
   
})

test('check getRandomData function by passing null', () => {
    let r;
    r = e.getRandomData(null);
//console.log(r);	

	expect(r.iv.words.length).toBe(0);
	expect(r.iv.words).toEqual([]);
	
	expect(r.iv.sigBytes).toBe(0);	

	expect(r.salt).toBe("");	
   
})

test('check getRandomData function by passing negative value', () => {
    let r;
    r = e.getRandomData(-16);
//console.log(r);	

	expect(r.iv.words.length).toBe(0);
	expect(r.iv.words).toEqual([]);
	
	expect(r.iv.sigBytes).toBe(-16);	

	expect(r.salt).toBe("");
   
})


